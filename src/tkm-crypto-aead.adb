--
--  Copyright (C) 2020  Tobias Brunner <tobias@codelabs.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Unchecked_Conversion;

with LSC.AES.CBC;
with LSC.Types;

with Tkm.Crypto.Hmac_Sha512;

package body Tkm.Crypto.Aead
is

   function Convert_Key
     (Key : Tkmrpc.Types.Key_Type)
      return LSC.AES.Key_Type;

   function Message_To_Sequence
     (Message : LSC.AES.Message_Type)
      return Tkmrpc.Types.Byte_Sequence;

   function Sequence_To_Block
     (Sequence : Tkmrpc.Types.Byte_Sequence)
      return LSC.AES.Block_Type;

   function Sequence_To_Message
     (Sequence : Tkmrpc.Types.Byte_Sequence)
      return LSC.AES.Message_Type;

   -------------------------------------------------------------------------

   function Convert_Key
     (Key : Tkmrpc.Types.Key_Type)
      return LSC.AES.Key_Type
   is
      subtype Target is
        LSC.AES.Key_Type (0 .. LSC.Types.Index (Key.Size / 4 - 1));

      function To_Target is new Ada.Unchecked_Conversion
        (Source => Tkmrpc.Types.Byte_Sequence,
         Target => Target);
   begin
      return To_Target (Key.Data);
   end Convert_Key;

   -------------------------------------------------------------------------

   procedure Create
     (Isa_Id   : Tkmrpc.Types.Isa_Id_Type;
      Sk_A_In  : Tkmrpc.Types.Key_Type;
      Sk_A_Out : Tkmrpc.Types.Key_Type;
      Sk_E_In  : Tkmrpc.Types.Key_Type;
      Sk_E_Out : Tkmrpc.Types.Key_Type)
   is
   begin
      Context_Array (Isa_Id).Sk_A_In  := Sk_A_In;
      Context_Array (Isa_Id).Sk_A_Out := Sk_A_Out;
      Context_Array (Isa_Id).Sk_E_In  := Sk_E_In;
      Context_Array (Isa_Id).Sk_E_Out := Sk_E_Out;
   end Create;

   -------------------------------------------------------------------------

   procedure Decrypt
     (Isa_Id               :     Tkmrpc.Types.Isa_Id_Type;
      Aad_Len              :     Tkmrpc.Types.Aad_Len_Type;
      Aad_Iv_Encrypted_Icv :     Tkmrpc.Types.Aad_Iv_Encrypted_Icv_Type;
      Decrypted            : out Tkmrpc.Types.Decrypted_Type)
   is
      use type Tkmrpc.Types.Byte_Sequence;

      Context   : constant Aead_Context_Type := Context_Array (Isa_Id);
      Plain_Len : constant Natural
        := Natural (Aad_Iv_Encrypted_Icv.Size) - Natural (Aad_Len) -
                    Iv_Length - Icv_Length;

      Hmac      : Crypto.Hmac_Sha512.Context_Type;
      Icv_Full  : Tkmrpc.Types.Byte_Sequence
        (1 .. Crypto.Hmac_Sha512.Hash_Output_Length);
      Aes       : LSC.AES.AES_Dec_Context;
      Plaintext : LSC.AES.Message_Type (1 .. Plain_Len / 16);
   begin
      if Context = Null_Aead_Context then
         raise Aead_Error with "ISA context" & Isa_Id'Img & " not found";
      end if;

      Crypto.Hmac_Sha512.Init
        (Ctx => Hmac,
         Key => Context.Sk_A_In.Data
           (Context.Sk_A_In.Data'First ..
                Context.Sk_A_In.Data'First + Context.Sk_A_In.Size - 1));
      Icv_Full := Crypto.Hmac_Sha512.Generate
        (Ctx  => Hmac,
         Data => Aad_Iv_Encrypted_Icv.Data
           (Aad_Iv_Encrypted_Icv.Data'First ..
                Aad_Iv_Encrypted_Icv.Data'First +
                  Aad_Iv_Encrypted_Icv.Size - Icv_Length - 1));

      if Aad_Iv_Encrypted_Icv.Data
        (Aad_Iv_Encrypted_Icv.Data'First + Aad_Iv_Encrypted_Icv.Size -
           Icv_Length .. Aad_Iv_Encrypted_Icv.Data'First +
             Aad_Iv_Encrypted_Icv.Size - 1) /=
            Icv_Full (Icv_Full'First .. Icv_Full'First + Icv_Length - 1)
      then
         raise Aead_Error with "Integrity check for message on ISA"
           & Isa_Id'Img & " failed";
      end if;

      Aes := LSC.AES.Create_AES256_Dec_Context
        (Key => Convert_Key (Context.Sk_E_In));

      LSC.AES.CBC.Decrypt
        (Context    => Aes,
         IV         => Sequence_To_Block
           (Sequence => Aad_Iv_Encrypted_Icv.Data
                (Aad_Iv_Encrypted_Icv.Data'First + Natural (Aad_Len) ..
                       Aad_Iv_Encrypted_Icv.Data'First + Natural (Aad_Len) +
                       Iv_Length - 1)),
         Ciphertext => Sequence_To_Message
           (Sequence => Aad_Iv_Encrypted_Icv.Data
                (Aad_Iv_Encrypted_Icv.Data'First +
                     Natural (Aad_Len) + Iv_Length ..
                       Aad_Iv_Encrypted_Icv.Data'First +
                         Aad_Iv_Encrypted_Icv.Size - Icv_Length - 1)),
         Length     => Plaintext'Length,
         Plaintext  => Plaintext);

      Decrypted.Size := Plain_Len;
      Decrypted.Data := (others => 0);
      Decrypted.Data
        (Decrypted.Data'First .. Decrypted.Data'First + Plain_Len - 1)
        := Message_To_Sequence (Message => Plaintext);
   end Decrypt;

   -------------------------------------------------------------------------

   procedure Encrypt
     (Isa_Id           :     Tkmrpc.Types.Isa_Id_Type;
      Iv               :     Tkmrpc.Types.Byte_Sequence;
      Aad_Len          :     Tkmrpc.Types.Aad_Len_Type;
      Aad_Plain        :     Tkmrpc.Types.Aad_Plain_Type;
      Iv_Encrypted_Icv : out Tkmrpc.Types.Iv_Encrypted_Icv_Type)
   is
      use type Tkmrpc.Types.Aad_Len_Type;

      Context    : constant Aead_Context_Type := Context_Array (Isa_Id);
      Plain_Len  : constant Natural
        := Natural (Aad_Plain.Size) - Natural (Aad_Len);

      Signed     : Tkmrpc.Types.Aad_Iv_Encrypted_Icv_Type;
      Hmac       : Crypto.Hmac_Sha512.Context_Type;
      Icv_Full   : Tkmrpc.Types.Byte_Sequence
        (1 .. Crypto.Hmac_Sha512.Hash_Output_Length);
      Aes        : LSC.AES.AES_Enc_Context;
      Ciphertext : LSC.AES.Message_Type (1 .. Plain_Len / 16);
   begin
      if Context = Null_Aead_Context then
         raise Aead_Error with "ISA context" & Isa_Id'Img & " not found";
      end if;

      --  Authenticated are AAD, IV and ciphertext
      if Aad_Len > 0 then
         Signed.Size := Natural (Aad_Len);
         Signed.Data (Signed.Data'First .. Signed.Data'First + Signed.Size - 1)
           := Aad_Plain.Data
             (Aad_Plain.Data'First .. Aad_Plain.Data'First + Signed.Size - 1);
         Signed.Data (Signed.Data'First + Signed.Size ..
                        Signed.Data'First + Signed.Size + Iv'Length - 1)
             := Iv;
         Signed.Size := Signed.Size + Iv'Length;
      else
         Signed.Data (Signed.Data'First .. Signed.Data'First + Iv'Length - 1)
             := Iv;
         Signed.Size := Iv'Length;
      end if;

      Aes := LSC.AES.Create_AES256_Enc_Context
        (Key => Convert_Key (Context.Sk_E_Out));

      LSC.AES.CBC.Encrypt
        (Context    => Aes,
         IV         => Sequence_To_Block (Sequence => Iv),
         Plaintext  => Sequence_To_Message
           (Sequence => Aad_Plain.Data
                (Aad_Plain.Data'First + Natural (Aad_Len) ..
                       Aad_Plain.Data'First + Aad_Plain.Size - 1)),
         Length     => Ciphertext'Length,
         Ciphertext => Ciphertext);

      Signed.Data (Signed.Data'First + Signed.Size ..
                     Signed.Data'First + Signed.Size + Plain_Len - 1)
          := Message_To_Sequence (Ciphertext);
      Signed.Size := Signed.Size + Plain_Len;

      Crypto.Hmac_Sha512.Init
        (Ctx => Hmac,
         Key => Context.Sk_A_Out.Data
           (Context.Sk_A_Out.Data'First ..
                Context.Sk_A_Out.Data'First + Context.Sk_A_Out.Size - 1));
      Icv_Full := Crypto.Hmac_Sha512.Generate
            (Ctx  => Hmac,
             Data => Signed.Data
               (Signed.Data'First .. Signed.Data'First + Signed.Size - 1));

      Iv_Encrypted_Icv.Size := Signed.Size - Natural (Aad_Len);
      Iv_Encrypted_Icv.Data := (others => 0);
      Iv_Encrypted_Icv.Data
        (Iv_Encrypted_Icv.Data'First ..
           Iv_Encrypted_Icv.Data'First + Signed.Size - Natural (Aad_Len) - 1)
          := Signed.Data (Signed.Data'First + Natural (Aad_Len) ..
                                Signed.Data'First + Signed.Size - 1);
      Iv_Encrypted_Icv.Data
        (Iv_Encrypted_Icv.Data'First + Iv_Encrypted_Icv.Size ..
           Iv_Encrypted_Icv.Data'First + Iv_Encrypted_Icv.Size +
             Icv_Length - 1)
            := Icv_Full (Icv_Full'First .. Icv_Full'First + Icv_Length - 1);
      Iv_Encrypted_Icv.Size := Iv_Encrypted_Icv.Size + Icv_Length;
   end Encrypt;

   -------------------------------------------------------------------------

   function Message_To_Sequence
     (Message : LSC.AES.Message_Type)
      return Tkmrpc.Types.Byte_Sequence
   is
      subtype Sequence is Tkmrpc.Types.Byte_Sequence
        (1 .. Message'Length * 16);

      function To_Sequence is new Ada.Unchecked_Conversion
        (Source => LSC.AES.Message_Type,
         Target => Sequence);
   begin
      return To_Sequence (Message);
   end Message_To_Sequence;

   -------------------------------------------------------------------------

   procedure Reset (Isa_Id : Tkmrpc.Types.Isa_Id_Type)
   is
   begin
      Context_Array (Isa_Id) := Null_Aead_Context;
   end Reset;

   -------------------------------------------------------------------------

   function Sequence_To_Block
     (Sequence : Tkmrpc.Types.Byte_Sequence)
      return LSC.AES.Block_Type
   is
      function To_Block is new Ada.Unchecked_Conversion
        (Source => Tkmrpc.Types.Byte_Sequence,
         Target => LSC.AES.Block_Type);
   begin
      return To_Block (Sequence);
   end Sequence_To_Block;

   -------------------------------------------------------------------------

   function Sequence_To_Message
     (Sequence : Tkmrpc.Types.Byte_Sequence)
      return LSC.AES.Message_Type
   is
      subtype Message is LSC.AES.Message_Type (1 .. Sequence'Length / 16);

      function To_Message is new Ada.Unchecked_Conversion
        (Source => Tkmrpc.Types.Byte_Sequence,
         Target => Message);
   begin
      return To_Message (Sequence);
   end Sequence_To_Message;

end Tkm.Crypto.Aead;
