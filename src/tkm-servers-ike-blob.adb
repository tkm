--
--  Copyright (C) 2021  Tobias Brunner <tobias@codelabs.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tkm.Logger;

package body Tkm.Servers.Ike.Blob
is

   package L renames Tkm.Logger;

   use type Tkmrpc.Types.Blob_Length_Type;
   use type Tkmrpc.Types.Blob_Offset_Type;

   --  Single buffer for data of all blob contexts
   subtype Buffer_Length_Type is
     Tkmrpc.Types.Sequence_Range range 0 .. 128 * 1024;
   subtype Buffer_Range_Type is
     Tkmrpc.Types.Sequence_Range range 0 .. Buffer_Length_Type'Last - 1;
   subtype Buffer_Type is Tkmrpc.Types.U8_Sequence (Buffer_Range_Type);

   Used_Length : Buffer_Length_Type := 0;
   Buffer      : Buffer_Type := (others => 0);

   --  Data stored for each blob context (points into the buffer above)
   type Blob_Context_Type is record
      State  : Blob_State_Type;
      Index  : Buffer_Range_Type;
      Length : Buffer_Length_Type;
   end record;

   Null_Blob_Context : constant Blob_Context_Type
     := (State  => Clean,
         Index  => Buffer_Range_Type'First,
         Length => Buffer_Length_Type'First);

   type Blob_Contexts_Type is array (Tkmrpc.Types.Blob_Id_Type)
     of Blob_Context_Type;

   Blob_Contexts : Blob_Contexts_Type := (others => Null_Blob_Context);

   -------------------------------------------------------------------------

   procedure Create
     (Id     : Tkmrpc.Types.Blob_Id_Type;
      Length : Tkmrpc.Types.Blob_Length_Type)
   is
   begin
      if Blob_Contexts (Id).State /= Clean then
         raise Blob_Failure with "Blob context with ID" & Id'Img
           & " already in use";
      elsif Length > Tkmrpc.Types.Blob_Length_Type (Buffer_Length_Type'Last)
        or else Buffer_Length_Type (Length) > Buffer'Length - Used_Length
      then
         raise Blob_Failure with "Unable to create blob context with ID"
           & Id'Img & " and length" & Length'Img;
      end if;

      L.Log (Message => "Creating new blob context with ID" & Id'Img
             & " (length" & Length'Img & ")");

      Blob_Contexts (Id) :=
        (State  => Created,
         Index  => Used_Length,
         Length => Buffer_Length_Type (Length));

      Used_Length := Used_Length + Buffer_Length_Type (Length);
   end Create;

   -------------------------------------------------------------------------

   function Get_Length
     (Id : Tkmrpc.Types.Blob_Id_Type)
      return Tkmrpc.Types.Blob_Length_Type
   is (Tkmrpc.Types.Blob_Length_Type (Blob_Contexts (Id).Length));

   -------------------------------------------------------------------------

   function Get_State
     (Id : Tkmrpc.Types.Blob_Id_Type)
      return Blob_State_Type
   is (Blob_Contexts (Id).State);

   -------------------------------------------------------------------------

   procedure Read
     (Id     :     Tkmrpc.Types.Blob_Id_Type;
      Offset :     Tkmrpc.Types.Blob_Offset_Type;
      Length :     Tkmrpc.Types.Blob_Length_Type;
      Data   : out Tkmrpc.Types.Byte_Sequence)
   is
   begin
      if Blob_Contexts (Id).State = Clean then
         raise Blob_Failure with "Blob context with ID" & Id'Img
           & " is not initialized";
      elsif Offset > Tkmrpc.Types.Blob_Offset_Type (Blob_Contexts (Id).Length)
      then
         raise Blob_Failure with "Invalid offset when reading from blob"
           & " context with ID" & Id'Img;
      elsif  Length > Tkmrpc.Types.Blob_Length_Type
        (Blob_Contexts (Id).Length) - Tkmrpc.Types.Blob_Length_Type (Offset)
        or else Length > Data'Length
      then
         raise Blob_Failure with "Invalid length when reading from blob"
           & " context with ID" & Id'Img;
      end if;

      Data := (others => 0);
      Data (Data'First .. Data'First + Natural (Length) - 1)
        := Buffer (Blob_Contexts (Id).Index + Natural (Offset) ..
                       Blob_Contexts (Id).Index + Natural (Offset) +
                       Natural (Length) - 1);
   end Read;

   -------------------------------------------------------------------------

   procedure Reset
     (Id : Tkmrpc.Types.Blob_Id_Type)
   is
      Index  : constant Buffer_Range_Type  := Blob_Contexts (Id).Index;
      Length : constant Buffer_Length_Type := Blob_Contexts (Id).Length;
   begin
      if Length > 0 then
         for I in Buffer_Range_Type range Index + Length .. Used_Length - 1
         loop
            Buffer (I - Length) := Buffer (I);

            if I >= Used_Length - Length then
               Buffer (I) := 0;
            end if;
         end loop;

         for I in Blob_Contexts'Range
         loop
            if Blob_Contexts (I).Index >= Index + Length then
               Blob_Contexts (I).Index := Blob_Contexts (I).Index - Length;
            end if;
         end loop;

         Used_Length := Used_Length - Length;
      end if;

      Blob_Contexts (Id) := Null_Blob_Context;
   end Reset;

   -------------------------------------------------------------------------

   procedure Write
     (Id     : Tkmrpc.Types.Blob_Id_Type;
      Offset : Tkmrpc.Types.Blob_Offset_Type;
      Data   : Tkmrpc.Types.Byte_Sequence)
   is
   begin
      if Blob_Contexts (Id).State = Clean then
         raise Blob_Failure with "Blob context with ID" & Id'Img
           & " is not initialized";
      elsif Offset > Tkmrpc.Types.Blob_Offset_Type (Blob_Contexts (Id).Length)
      then
         raise Blob_Failure with "Invalid offset when writing to blob"
           & " context with ID" & Id'Img;
      elsif Data'Length > Blob_Contexts (Id).Length -
        Buffer_Length_Type (Offset)
      then
         raise Blob_Failure with "Invalid length when writing to blob"
           & " context with ID" & Id'Img;
      end if;

      Buffer (Blob_Contexts (Id).Index + Natural (Offset)
              .. Blob_Contexts (Id).Index + Natural (Offset) + Data'Length - 1)
        := Data;
   end Write;

end Tkm.Servers.Ike.Blob;
