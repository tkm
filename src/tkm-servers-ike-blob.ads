--
--  Copyright (C) 2021  Tobias Brunner <tobias@codelabs.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tkmrpc.Types;

package Tkm.Servers.Ike.Blob
is
   type Blob_State_Type is
     (Clean,
      Created);
   --  State of a blob context.

   function Get_State
     (Id : Tkmrpc.Types.Blob_Id_Type)
      return Blob_State_Type;
   --  Returns the state of the blob context with the given id.

   function Get_Length
     (Id : Tkmrpc.Types.Blob_Id_Type)
      return Tkmrpc.Types.Blob_Length_Type;
   --  Returns the length of the blob context with the given id.

   procedure Create
     (Id     : Tkmrpc.Types.Blob_Id_Type;
      Length : Tkmrpc.Types.Blob_Length_Type);
   --  Create a new blob context with given id and length. Raises a
   --  Blob_Failure if not enough space is available or if the context
   --  is already in use.

   procedure Read
     (Id     :     Tkmrpc.Types.Blob_Id_Type;
      Offset :     Tkmrpc.Types.Blob_Offset_Type;
      Length :     Tkmrpc.Types.Blob_Length_Type;
      Data   : out Tkmrpc.Types.Byte_Sequence);
   --  Read data from the given blob context. Raises a Blob_Failure if the
   --  blob context's state or the given offset or length are invalid.

   procedure Write
     (Id     : Tkmrpc.Types.Blob_Id_Type;
      Offset : Tkmrpc.Types.Blob_Offset_Type;
      Data   : Tkmrpc.Types.Byte_Sequence);
   --  Write data to the given blob context. Raises a Blob_Failure if the
   --  blob context's state or the given offset or length are invalid.

   procedure Reset
     (Id : Tkmrpc.Types.Blob_Id_Type);
   --  Resets the blob context with the given id.

   Blob_Failure : exception;

end Tkm.Servers.Ike.Blob;
