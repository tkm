--
--  Copyright (C) 2020  Tobias Brunner <tobias@codelabs.ch>
--  Copyright (C) 2013  Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2013  Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tkmrpc.Constants;
with Tkmrpc.Contexts.ae;
with Tkmrpc.Contexts.ke;
with Tkmrpc.Contexts.isa;
with Tkmrpc.Contexts.nc;
with Tkmrpc.Contexts.esa;
with Tkmrpc.Contexts.cc;

with Tkm.Logger;
with Tkm.Crypto.Aead;
with Tkm.Crypto.Random;
with Tkm.Servers.Ike.Nonce;
with Tkm.Servers.Ike.Ke;
with Tkm.Servers.Ike.Ae;
with Tkm.Servers.Ike.Isa;
with Tkm.Servers.Ike.Esa;
with Tkm.Servers.Ike.Cc;
with Tkm.Servers.Ike.Blob;

package body Tkmrpc.Servers.Ike
is

   use Tkm;

   package L renames Tkm.Logger;

   -------------------------------------------------------------------------

   procedure Ae_Reset
     (Result : out Results.Result_Type;
      Ae_Id  : Types.Ae_Id_Type)
   is
   begin
      Tkm.Servers.Ike.Ae.Reset (Ae_Id => Ae_Id);
      Result := Results.Ok;
   end Ae_Reset;

   -------------------------------------------------------------------------

   procedure Blob_Create
     (Result  : out Results.Result_Type;
      Blob_Id :     Types.Blob_Id_Type;
      Length  :     Types.Blob_Length_Type)
   is
   begin
      Tkm.Servers.Ike.Blob.Create
        (Id      => Blob_Id,
         Length  => Length);
      Result := Results.Ok;
   end Blob_Create;

   -------------------------------------------------------------------------

   procedure Blob_Read
     (Result    : out Results.Result_Type;
      Blob_Id   :     Types.Blob_Id_Type;
      Offset    :     Types.Blob_Offset_Type;
      Length    :     Types.Blob_Length_Type;
      Blob_Data : out Types.Blob_Out_Bytes_Type)
   is
   begin
      Tkm.Servers.Ike.Blob.Read
        (Id     => Blob_Id,
         Offset => Offset,
         Length => Length,
         Data   => Types.Byte_Sequence (Blob_Data));
      Result := Results.Ok;
   end Blob_Read;

   -------------------------------------------------------------------------

   procedure Blob_Reset
     (Result  : out Results.Result_Type;
      Blob_Id :     Types.Blob_Id_Type)
   is
   begin
      Tkm.Servers.Ike.Blob.Reset (Id => Blob_Id);
      Result := Results.Ok;
   end Blob_Reset;

   -------------------------------------------------------------------------

   procedure Blob_Write
     (Result    : out Results.Result_Type;
      Blob_Id   :     Types.Blob_Id_Type;
      Offset    :     Types.Blob_Offset_Type;
      Blob_Data :     Types.Blob_In_Bytes_Type)
   is
   begin
      Tkm.Servers.Ike.Blob.Write
        (Id     => Blob_Id,
         Offset => Offset,
         Data   => Blob_Data.Data
           (Blob_Data.Data'First .. Blob_Data.Data'First +
                Blob_Data.Size - 1));
      Result := Results.Ok;
   end Blob_Write;

   -------------------------------------------------------------------------

   procedure Cc_Add_Certificate
     (Result      : out Results.Result_Type;
      Cc_Id       : Types.Cc_Id_Type;
      Autha_Id    : Types.Autha_Id_Type;
      Certificate : Types.Certificate_Type)
   is
   begin
      Tkm.Servers.Ike.Cc.Add_Certificate
        (Cc_Id       => Cc_Id,
         Autha_Id    => Autha_Id,
         Certificate => Certificate);
      Result := Results.Ok;
   end Cc_Add_Certificate;

   -------------------------------------------------------------------------

   procedure Cc_Check_Ca
     (Result         : out Results.Result_Type;
      Cc_Id          :     Types.Cc_Id_Type;
      Ca_Id          :     Types.Ca_Id_Type;
      Ca_Certificate :     Types.Certificate_Type)
   is
   begin
      Tkm.Servers.Ike.Cc.Check_Ca
        (Cc_Id          => Cc_Id,
         Ca_Id          => Ca_Id,
         Ca_Certificate => Ca_Certificate);
      Result := Results.Ok;
   end Cc_Check_Ca;

   -------------------------------------------------------------------------

   procedure Cc_Check_Chain
     (Result : out Results.Result_Type;
      Cc_Id  :     Types.Cc_Id_Type;
      Ri_Id  :     Types.Ri_Id_Type)
   is
   begin
      Tkm.Servers.Ike.Cc.Check_Chain
        (Cc_Id => Cc_Id,
         Ri_Id => Ri_Id);
      Result := Results.Ok;
   end Cc_Check_Chain;

   -------------------------------------------------------------------------

   procedure Cc_Reset
     (Result : out Results.Result_Type;
      Cc_Id  : Types.Cc_Id_Type)
   is
   begin
      Tkm.Servers.Ike.Cc.Reset (Cc_Id => Cc_Id);
      Result := Results.Ok;
   end Cc_Reset;

   -------------------------------------------------------------------------

   procedure Esa_Create
     (Result      : out Results.Result_Type;
      Esa_Id      : Types.Esa_Id_Type;
      Isa_Id      : Types.Isa_Id_Type;
      Sp_Id       : Types.Sp_Id_Type;
      Ea_Id       : Types.Ea_Id_Type;
      Ke_Ids      : Types.Ke_Ids_Type;
      Nc_Loc_Id   : Types.Nc_Id_Type;
      Nonce_Rem   : Types.Nonce_Type;
      Esa_Flags   : Types.Esa_Flags_Type;
      Esp_Spi_Loc : Types.Esp_Spi_Type;
      Esp_Spi_Rem : Types.Esp_Spi_Type)
   is
   begin
      Tkm.Servers.Ike.Esa.Create
        (Esa_Id      => Esa_Id,
         Isa_Id      => Isa_Id,
         Sp_Id       => Sp_Id,
         Ea_Id       => Ea_Id,
         Ke_Ids      => Ke_Ids,
         Nc_Loc_Id   => Nc_Loc_Id,
         Nonce_Rem   => Nonce_Rem,
         Esa_Flags   => Esa_Flags,
         Esp_Spi_Loc => Esp_Spi_Loc,
         Esp_Spi_Rem => Esp_Spi_Rem);
      Result := Results.Ok;
   end Esa_Create;

   -------------------------------------------------------------------------

   procedure Esa_Create_First
     (Result      : out Results.Result_Type;
      Esa_Id      : Types.Esa_Id_Type;
      Isa_Id      : Types.Isa_Id_Type;
      Sp_Id       : Types.Sp_Id_Type;
      Ea_Id       : Types.Ea_Id_Type;
      Esa_Flags   : Types.Esa_Flags_Type;
      Esp_Spi_Loc : Types.Esp_Spi_Type;
      Esp_Spi_Rem : Types.Esp_Spi_Type)
   is
      pragma Unreferenced (Esa_Flags);
   begin
      Tkm.Servers.Ike.Esa.Create_First
        (Esa_Id      => Esa_Id,
         Isa_Id      => Isa_Id,
         Sp_Id       => Sp_Id,
         Ea_Id       => Ea_Id,
         Esp_Spi_Loc => Esp_Spi_Loc,
         Esp_Spi_Rem => Esp_Spi_Rem);
      Result := Results.Ok;
   end Esa_Create_First;

   -------------------------------------------------------------------------

   procedure Esa_Create_No_Pfs
     (Result      : out Results.Result_Type;
      Esa_Id      : Types.Esa_Id_Type;
      Isa_Id      : Types.Isa_Id_Type;
      Sp_Id       : Types.Sp_Id_Type;
      Ea_Id       : Types.Ea_Id_Type;
      Nc_Loc_Id   : Types.Nc_Id_Type;
      Nonce_Rem   : Types.Nonce_Type;
      Esa_Flags   : Types.Esa_Flags_Type;
      Esp_Spi_Loc : Types.Esp_Spi_Type;
      Esp_Spi_Rem : Types.Esp_Spi_Type)
   is
   begin
      Tkm.Servers.Ike.Esa.Create_No_Pfs
        (Esa_Id      => Esa_Id,
         Isa_Id      => Isa_Id,
         Sp_Id       => Sp_Id,
         Ea_Id       => Ea_Id,
         Nc_Loc_Id   => Nc_Loc_Id,
         Nonce_Rem   => Nonce_Rem,
         Esa_Flags   => Esa_Flags,
         Esp_Spi_Loc => Esp_Spi_Loc,
         Esp_Spi_Rem => Esp_Spi_Rem);
      Result := Results.Ok;
   end Esa_Create_No_Pfs;

   -------------------------------------------------------------------------

   procedure Esa_Reset
     (Result : out Results.Result_Type;
      Esa_Id : Types.Esa_Id_Type)
   is
   begin
      Tkm.Servers.Ike.Esa.Reset (Esa_Id => Esa_Id);
      Result := Results.Ok;
   end Esa_Reset;

   -------------------------------------------------------------------------

   procedure Esa_Select
     (Result : out Results.Result_Type;
      Esa_Id : Types.Esa_Id_Type)
   is
      pragma Unreferenced (Esa_Id);
   begin

      --  Since charon's behavior makes sure that old child SAs are deleted and
      --  that only the new ESP SA is present, there is no need for additional
      --  steps by the TKM.

      Result := Results.Ok;
   end Esa_Select;

   -------------------------------------------------------------------------

   procedure Finalize
   is
   begin
      Crypto.Random.Finalize;
   end Finalize;

   -------------------------------------------------------------------------

   procedure Init
   is
   begin
      Crypto.Random.Init;
   end Init;

   -------------------------------------------------------------------------

   procedure Isa_Auth
     (Result       : out Results.Result_Type;
      Isa_Id       : Types.Isa_Id_Type;
      Cc_Id        : Types.Cc_Id_Type;
      Init_Message : Types.Init_Message_Type;
      Signature    : Types.Signature_Type)
   is
   begin
      Tkm.Servers.Ike.Isa.Auth (Isa_Id       => Isa_Id,
                                Cc_Id        => Cc_Id,
                                Init_Message => Init_Message,
                                Signature    => Signature);
      Result := Results.Ok;
   end Isa_Auth;

   -------------------------------------------------------------------------

   procedure Isa_Create
     (Result    : out Results.Result_Type;
      Isa_Id    : Types.Isa_Id_Type;
      Ae_Id     : Types.Ae_Id_Type;
      Ia_Id     : Types.Ia_Id_Type;
      Ke_Id     : Types.Ke_Id_Type;
      Nc_Loc_Id : Types.Nc_Id_Type;
      Nonce_Rem : Types.Nonce_Type;
      Initiator : Types.Init_Type;
      Spi_Loc   : Types.Ike_Spi_Type;
      Spi_Rem   : Types.Ike_Spi_Type;
      Block_Len : out Types.Block_Len_Type;
      Icv_Len   : out Types.Icv_Len_Type;
      Iv_Len    : out Types.Iv_Len_Type)
   is
   begin
      Tkm.Servers.Ike.Isa.Create
        (Isa_Id    => Isa_Id,
         Ae_Id     => Ae_Id,
         Ia_Id     => Ia_Id,
         Ke_Id     => Ke_Id,
         Nc_Loc_Id => Nc_Loc_Id,
         Nonce_Rem => Nonce_Rem,
         Initiator => Initiator,
         Spi_Loc   => Spi_Loc,
         Spi_Rem   => Spi_Rem,
         Block_Len => Block_Len,
         Icv_Len   => Icv_Len,
         Iv_Len    => Iv_Len);
      Result := Results.Ok;
   end Isa_Create;

   -------------------------------------------------------------------------

   procedure Isa_Create_Child
     (Result        : out Results.Result_Type;
      Isa_Id        : Types.Isa_Id_Type;
      Parent_Isa_Id : Types.Isa_Id_Type;
      Ia_Id         : Types.Ia_Id_Type;
      Ke_Ids        : Types.Ke_Ids_Type;
      Nc_Loc_Id     : Types.Nc_Id_Type;
      Nonce_Rem     : Types.Nonce_Type;
      Initiator     : Types.Init_Type;
      Spi_Loc       : Types.Ike_Spi_Type;
      Spi_Rem       : Types.Ike_Spi_Type;
      Block_Len     : out Types.Block_Len_Type;
      Icv_Len       : out Types.Icv_Len_Type;
      Iv_Len        : out Types.Iv_Len_Type)
   is
   begin
      Tkm.Servers.Ike.Isa.Create_Child
        (Isa_Id        => Isa_Id,
         Parent_Isa_Id => Parent_Isa_Id,
         Ia_Id         => Ia_Id,
         Ke_Ids        => Ke_Ids,
         Nc_Loc_Id     => Nc_Loc_Id,
         Nonce_Rem     => Nonce_Rem,
         Initiator     => Initiator,
         Spi_Loc       => Spi_Loc,
         Spi_Rem       => Spi_Rem,
         Block_Len     => Block_Len,
         Icv_Len       => Icv_Len,
         Iv_Len        => Iv_Len);
      Result := Results.Ok;
   end Isa_Create_Child;

   -------------------------------------------------------------------------

   procedure Isa_Decrypt
     (Result               : out Results.Result_Type;
      Isa_Id               : Types.Isa_Id_Type;
      Aad_Len              : Types.Aad_Len_Type;
      Aad_Iv_Encrypted_Icv : Types.Aad_Iv_Encrypted_Icv_Type;
      Decrypted            : out Types.Decrypted_Type)
   is
   begin
      Tkm.Crypto.Aead.Decrypt
        (Isa_Id               => Isa_Id,
         Aad_Len              => Aad_Len,
         Aad_Iv_Encrypted_Icv => Aad_Iv_Encrypted_Icv,
         Decrypted            => Decrypted);
      Result := Results.Ok;
   end Isa_Decrypt;

   -------------------------------------------------------------------------

   procedure Isa_Encrypt
     (Result           : out Results.Result_Type;
      Isa_Id           : Types.Isa_Id_Type;
      Aad_Len          : Types.Aad_Len_Type;
      Aad_Plain        : Types.Aad_Plain_Type;
      Iv_Encrypted_Icv : out Types.Iv_Encrypted_Icv_Type)
   is
      Iv : Tkmrpc.Types.Byte_Sequence (1 .. Tkm.Crypto.Aead.Iv_Length);
   begin
      Iv := Tkm.Crypto.Random.Get (Size => Iv'Length);
      Tkm.Crypto.Aead.Encrypt
        (Isa_Id           => Isa_Id,
         Iv               => Iv,
         Aad_Len          => Aad_Len,
         Aad_Plain        => Aad_Plain,
         Iv_Encrypted_Icv => Iv_Encrypted_Icv);
      Result := Results.Ok;
   end Isa_Encrypt;

   -------------------------------------------------------------------------

   procedure Isa_Int_Auth
     (Result  : out Results.Result_Type;
      Isa_Id  :     Types.Isa_Id_Type;
      Inbound :     Types.Inbound_Flag_Type;
      Data_Id :     Types.Blob_Id_Type)
   is
   begin
      Tkm.Servers.Ike.Isa.Int_Auth
        (Isa_Id  => Isa_Id,
         Inbound => Inbound,
         Data_Id => Data_Id);
      Result := Results.Ok;
   end Isa_Int_Auth;

   -------------------------------------------------------------------------

   procedure Isa_Reset
     (Result : out Results.Result_Type;
      Isa_Id : Types.Isa_Id_Type)
   is
   begin
      Tkm.Servers.Ike.Isa.Reset (Isa_Id => Isa_Id);
      Result := Results.Ok;
   end Isa_Reset;

   -------------------------------------------------------------------------

   procedure Isa_Sign
     (Result       : out Results.Result_Type;
      Isa_Id       : Types.Isa_Id_Type;
      Lc_Id        : Types.Lc_Id_Type;
      Init_Message : Types.Init_Message_Type;
      Signature    : out Types.Signature_Type)
   is
   begin
      Tkm.Servers.Ike.Isa.Sign (Isa_Id       => Isa_Id,
                                Lc_Id        => Lc_Id,
                                Init_Message => Init_Message,
                                Signature    => Signature);
      Result := Results.Ok;
   end Isa_Sign;

   -------------------------------------------------------------------------

   procedure Isa_Skip_Create_First
     (Result : out Results.Result_Type;
      Isa_Id : Types.Isa_Id_Type)
   is
   begin
      Tkm.Servers.Ike.Isa.Skip_Create_First (Isa_Id => Isa_Id);
      Result := Results.Ok;
   end Isa_Skip_Create_First;

   -------------------------------------------------------------------------

   procedure Isa_Update
     (Result : out Results.Result_Type;
      Isa_Id :     Types.Isa_Id_Type;
      Ke_Id  :     Types.Ke_Id_Type)
   is
   begin
      Tkm.Servers.Ike.Isa.Update
        (Isa_Id => Isa_Id,
         Ke_Id  => Ke_Id);
      Result := Results.Ok;
   end Isa_Update;

   -------------------------------------------------------------------------

   procedure Ke_Get
     (Result      : out Results.Result_Type;
      Ke_Id       : Types.Ke_Id_Type;
      Kea_Id      : Types.Kea_Id_Type;
      Pubvalue_Id : Types.Blob_Id_Type;
      Ke_Length   : out Types.Blob_Length_Type)
   is
   begin
      Ke_Length := Tkm.Servers.Ike.Ke.Get
        (Id          => Ke_Id,
         Kea_Id      => Kea_Id,
         Pubvalue_Id => Pubvalue_Id);
      Result := Results.Ok;
   end Ke_Get;

   -------------------------------------------------------------------------

   procedure Ke_Reset
     (Result : out Results.Result_Type;
      Ke_Id  : Types.Ke_Id_Type)
   is
   begin
      Tkm.Servers.Ike.Ke.Reset
        (Id       => Ke_Id);
      Result := Results.Ok;
   end Ke_Reset;

   -------------------------------------------------------------------------

   procedure Ke_Set
     (Result      : out Results.Result_Type;
      Ke_Id       : Types.Ke_Id_Type;
      Kea_Id      : Types.Kea_Id_Type;
      Pubvalue_Id : Types.Blob_Id_Type)
   is
   begin
      Tkm.Servers.Ike.Ke.Set
         (Id          => Ke_Id,
          Kea_Id      => Kea_Id,
          Pubvalue_Id => Pubvalue_Id);
      Result := Results.Ok;
   end Ke_Set;

   -------------------------------------------------------------------------

   procedure Nc_Create
     (Result       : out Results.Result_Type;
      Nc_Id        : Types.Nc_Id_Type;
      Nonce_Length : Types.Nonce_Length_Type;
      Nonce        : out Types.Nonce_Type)
   is
   begin
      Nonce := Tkm.Servers.Ike.Nonce.Create
        (Id     => Nc_Id,
         Length => Nonce_Length);
      Result := Results.Ok;
   end Nc_Create;

   -------------------------------------------------------------------------

   procedure Nc_Reset
     (Result : out Results.Result_Type;
      Nc_Id  : Types.Nc_Id_Type)
   is
   begin
      Tkm.Servers.Ike.Nonce.Reset (Id => Nc_Id);
      Result := Results.Ok;
   end Nc_Reset;

   -------------------------------------------------------------------------

   procedure Tkm_Limits
     (Result              : out Results.Result_Type;
      Max_Active_Requests : out Types.Active_Requests_Type;
      Nc_Contexts         : out Types.Nc_Id_Type;
      Ke_Contexts         : out Types.Ke_Id_Type;
      Cc_Contexts         : out Types.Cc_Id_Type;
      Ae_Contexts         : out Types.Ae_Id_Type;
      Isa_Contexts        : out Types.Isa_Id_Type;
      Esa_Contexts        : out Types.Esa_Id_Type;
      Blob_Contexts       : out Types.Blob_Id_Type)
   is
   begin

      --  Processing of requests in parallel is not yet supported.

      Max_Active_Requests := 1;

      Nc_Contexts   := Tkmrpc.Types.Nc_Id_Type'Last;
      Ke_Contexts   := Tkmrpc.Types.Ke_Id_Type'Last;
      Cc_Contexts   := Tkmrpc.Types.Cc_Id_Type'Last;
      Ae_Contexts   := Tkmrpc.Types.Ae_Id_Type'Last;
      Isa_Contexts  := Tkmrpc.Types.Isa_Id_Type'Last;
      Esa_Contexts  := Tkmrpc.Types.Esa_Id_Type'Last;
      Blob_Contexts := Tkmrpc.Types.Blob_Id_Type'Last;

      Result := Results.Ok;
   end Tkm_Limits;

   -------------------------------------------------------------------------

   procedure Tkm_Reset (Result : out Results.Result_Type)
   is
   begin
      L.Log (Message => "Resetting all contexts");

      for I in Tkmrpc.Types.Nc_Id_Type'Range loop
         Tkmrpc.Contexts.nc.reset (Id => I);
      end loop;
      for I in Tkmrpc.Types.Ke_Id_Type'Range loop
         Tkmrpc.Contexts.ke.reset (Id => I);
      end loop;
      for I in Tkmrpc.Types.Cc_Id_Type'Range loop
         Tkmrpc.Contexts.cc.reset (Id => I);
      end loop;
      for I in Tkmrpc.Types.Ae_Id_Type'Range loop
         Tkmrpc.Contexts.ae.reset (Id => I);
      end loop;
      for I in Tkmrpc.Types.Isa_Id_Type'Range loop
         Tkmrpc.Contexts.isa.reset (Id => I);
      end loop;
      for I in Tkmrpc.Types.Esa_Id_Type'Range loop
         Tkmrpc.Contexts.esa.reset (Id => I);
      end loop;
      for I in Tkmrpc.Types.Blob_Id_Type'Range loop
         Tkm.Servers.Ike.Blob.Reset (Id => I);
      end loop;

      Result := Results.Ok;
   end Tkm_Reset;

   -------------------------------------------------------------------------

   procedure Tkm_Version
     (Result  : out Results.Result_Type;
      Version : out Types.Version_Type)
   is
   begin
      Version := Tkmrpc.Constants.Ike_Version;
      Result  := Results.Ok;
   end Tkm_Version;

end Tkmrpc.Servers.Ike;
