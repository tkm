--
--  Copyright (C) 2013  Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2013  Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Strings.Fixed;

with X509.Validity;

with Tkm.Config;
with Tkm.Logger;

package body Tkm.Ca_Cert
is

   package L renames Tkm.Logger;

   use type X509.Certs.Certificate_Type;

   type CA_Certificate_Type is record
      ID   : Natural;
      Cert : X509.Certs.Certificate_Type;
   end record
     with Dynamic_Predicate =>
       (if CA_Certificate_Type.ID = 0 then
          CA_Certificate_Type.Cert =  X509.Certs.Null_Certificate);

   Null_CA : constant CA_Certificate_Type
     := (ID   => 0,
         Cert => X509.Certs.Null_Certificate);

   No_Cert_Idx : constant := 0;

   type Ca_Certs_Range is range
     No_Cert_Idx .. Config.Max_CA_Certificates_Count;
   subtype Valid_Ca_Certs_Range is
     Ca_Certs_Range range 1 .. Ca_Certs_Range'Last;

   Ca_Certs : array (Valid_Ca_Certs_Range) of CA_Certificate_Type
     := (others => Null_CA);

   procedure Load
     (CA_ID : Natural;
      Path  : String);
   --  Load CA certificate with specified CA ID from file given by path. Raises
   --  Ca_Not_Valid exception if the validity of the CA certificate(s) could
   --  not be verified.

   function Next_Free_Slot return Ca_Certs_Range;
   --  Returns the next free CA certificate storage slot. If no free slot was
   --  found, No_Cert_Idx is returned.

   -------------------------------------------------------------------------

   function Get (ID : Natural) return X509.Certs.Certificate_Type
   is
   begin
      for CA of Ca_Certs loop
         if CA.ID = ID then
            return CA.Cert;
         end if;
      end loop;

      raise Ca_Not_Found with "CA certificate with ID" & ID'Img
        & " not found";
   end Get;

   -------------------------------------------------------------------------

   procedure Load (Certificates : String)
   is
      Start_Idx, Colon_Idx, Comma_Idx : Natural;
   begin
      Comma_Idx := Certificates'First - 1;
      while Comma_Idx <= Certificates'Last loop
         Start_Idx := Comma_Idx + 1;
         Colon_Idx := Ada.Strings.Fixed.Index
           (Source  => Certificates,
            Pattern => ":",
            From    => Start_Idx);
         if Colon_Idx = 0 then
            raise Ca_Not_Valid with "Invalid certificates given '"
              & Certificates & "': missing ID";
         end if;
         Comma_Idx := Ada.Strings.Fixed.Index
           (Source  => Certificates,
            Pattern => ",",
            From    => Colon_Idx);
         if Comma_Idx = 0 then
            Comma_Idx := Certificates'Last + 1;
         end if;
         if Start_Idx >= Colon_Idx or Comma_Idx <= Colon_Idx + 1 then
            raise Ca_Not_Valid with "Invalid certificates given '"
              & Certificates & "'";
         end if;

         declare
            Path   : constant String
              := Certificates (Start_Idx .. Colon_Idx - 1);
            ID_Str : constant String
              := Certificates (Colon_Idx + 1 .. Comma_Idx - 1);
            ID : Natural;
         begin
            begin
               ID := Natural'Value (ID_Str);

            exception
               when others =>
                  raise Ca_Not_Valid with "Invalid ID '" & ID_Str
                    & "' for certificate '" & Path & "'";
            end;

            Load (CA_ID => ID,
                  Path  => Path);
         end;
      end loop;
   end Load;

   -------------------------------------------------------------------------

   procedure Load
     (CA_ID : Natural;
      Path  : String)
   is
      use X509;

      Entry_ID : constant Ca_Certs_Range := Next_Free_Slot;
   begin
      L.Log (Message => "Loading CA certificate '" & Path & "'");

      if Entry_ID = No_Cert_Idx then
         raise Ca_Not_Valid with "No free slot for CA certificate with ID"
           & CA_ID'Img;
      end if;

      begin
         Certs.Load (Filename => Path,
                     Cert     => Ca_Certs (Entry_ID).Cert);
         Ca_Certs (Entry_ID).ID := CA_ID;

         declare
            Subj : constant String := Certs.Get_Subject
              (Cert => Ca_Certs (Entry_ID).Cert);
            Val  : constant Validity.Validity_Type
              := Certs.Get_Validity (Cert => Ca_Certs (Entry_ID).Cert);
         begin
            if not Certs.Is_Ca (Cert => Ca_Certs (Entry_ID).Cert) then
               raise Ca_Not_Valid with "'" & Subj
                 & "' is not a CA certificate";
            end if;

            if not Validity.Is_Valid (V => Val) then
               raise Ca_Not_Valid with "'" & Subj & "' is not valid";
            end if;
         end;
         L.Log (Message => "CA certificate '" & X509.Certs.Get_Subject
                (Cert => Ca_Certs (Entry_ID).Cert) & "' with ID"
                & CA_ID'Img & " loaded");

      exception
         when others =>
            Ca_Certs (Entry_ID) := Null_CA;
            raise;
      end;
   end Load;

   -------------------------------------------------------------------------

   function Next_Free_Slot return Ca_Certs_Range
   is
   begin
      for I in Ca_Certs'Range loop
         if Ca_Certs (I) = Null_CA then
            return I;
         end if;
      end loop;

      return No_Cert_Idx;
   end Next_Free_Slot;

end Tkm.Ca_Cert;
