--
--  Copyright (C) 2020  Tobias Brunner <tobias@codelabs.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tkmrpc.Types;

package Tkm.Crypto.Aead
is

   Enc_Key_Length : constant := 32;
   --  Key length for 256-bit AES-CBC.
   Block_Length   : constant := 16;
   --  Block length for AES-CBC.
   Iv_Length      : constant := Block_Length;
   --  IV length for AES-CBC.
   Int_Key_Length : constant := 64;
   --  Key length for HMAC_SHA2_512_256.
   Icv_Length     : constant := 32;
   --  Output length of HMAC_SHA2_512_256.

   procedure Create
     (Isa_Id   : Tkmrpc.Types.Isa_Id_Type;
      Sk_A_In  : Tkmrpc.Types.Key_Type;
      Sk_A_Out : Tkmrpc.Types.Key_Type;
      Sk_E_In  : Tkmrpc.Types.Key_Type;
      Sk_E_Out : Tkmrpc.Types.Key_Type);
   --  Create an AEAD context with the given keys for an ISA context.

   procedure Decrypt
     (Isa_Id               :     Tkmrpc.Types.Isa_Id_Type;
      Aad_Len              :     Tkmrpc.Types.Aad_Len_Type;
      Aad_Iv_Encrypted_Icv :     Tkmrpc.Types.Aad_Iv_Encrypted_Icv_Type;
      Decrypted            : out Tkmrpc.Types.Decrypted_Type);
   --  Decrypt the given data for a given ISA context.

   procedure Encrypt
     (Isa_Id           :     Tkmrpc.Types.Isa_Id_Type;
      Iv               :     Tkmrpc.Types.Byte_Sequence;
      Aad_Len          :     Tkmrpc.Types.Aad_Len_Type;
      Aad_Plain        :     Tkmrpc.Types.Aad_Plain_Type;
      Iv_Encrypted_Icv : out Tkmrpc.Types.Iv_Encrypted_Icv_Type);
   --  Encrypt the given data for a given ISA context.

   procedure Reset (Isa_Id : Tkmrpc.Types.Isa_Id_Type);
   --  Reset the AEAD contexts for the given ISA context.

   Aead_Error : exception;

private

   type Aead_Context_Type is record
      Sk_A_In  : Tkmrpc.Types.Key_Type;
      Sk_A_Out : Tkmrpc.Types.Key_Type;
      Sk_E_In  : Tkmrpc.Types.Key_Type;
      Sk_E_Out : Tkmrpc.Types.Key_Type;
   end record;
   --  AEAD context data

   Null_Aead_Context : constant Aead_Context_Type
     := Aead_Context_Type'
       (Sk_A_In  => Tkmrpc.Types.Null_Key_Type,
        Sk_A_Out => Tkmrpc.Types.Null_Key_Type,
        Sk_E_In  => Tkmrpc.Types.Null_Key_Type,
        Sk_E_Out => Tkmrpc.Types.Null_Key_Type);

   type Context_Array_Type is
     array (Tkmrpc.Types.Isa_Id_Type) of Aead_Context_Type;

   Context_Array : Context_Array_Type := Context_Array_Type'
     (others => Null_Aead_Context);

end Tkm.Crypto.Aead;
