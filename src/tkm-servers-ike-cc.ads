--
--  Copyright (C) 2013  Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2013  Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tkmrpc.Types;

package Tkm.Servers.Ike.Cc
is

   procedure Add_Certificate
     (Cc_Id       : Tkmrpc.Types.Cc_Id_Type;
      Autha_Id    : Tkmrpc.Types.Autha_Id_Type;
      Certificate : Tkmrpc.Types.Certificate_Type);
   --  Add given certificate to certificate chain context specified by id.

   procedure Check_Ca
     (Cc_Id          : Tkmrpc.Types.Cc_Id_Type;
      Ca_Id          : Tkmrpc.Types.Ca_Id_Type;
      Ca_Certificate : Tkmrpc.Types.Certificate_Type);
   --  Check if trusted root CA specified by Ca_Id is identical to the given
   --  CA certificate. If True, link the specified certificate chain context to
   --  the specified root CA.

   procedure Check_Chain
     (Cc_Id : Tkmrpc.Types.Cc_Id_Type;
      Ri_Id : Tkmrpc.Types.Ri_Id_Type);
   --  Check certificate chain of context specified by cc_id with remote
   --  identity Ri_Id.

   procedure Reset (Cc_Id : Tkmrpc.Types.Cc_Id_Type);
   --  Reset certificate chain context with given id.

   Invalid_Certificate : exception;
   Invalid_Auth_Alg_ID : exception;

end Tkm.Servers.Ike.Cc;
