--
--  Copyright (C) 2021  Tobias Brunner <tobias@codelabs.ch>
--  Copyright (C) 2013  Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2013  Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tkmrpc.Types;

package Tkm.Servers.Ike.Ke
is

   function Get
     (Id          : Tkmrpc.Types.Ke_Id_Type;
      Kea_Id      : Tkmrpc.Types.Kea_Id_Type;
      Pubvalue_Id : Tkmrpc.Types.Blob_Id_Type)
      return Tkmrpc.Types.Blob_Length_Type;
   --  Get the public value for the given KE context using parameters of the
   --  specified algorithm. Returns the length of the public value written to
   --  the blob with the given id.

   procedure Set
     (Id          : Tkmrpc.Types.Ke_Id_Type;
      Kea_Id      : Tkmrpc.Types.Kea_Id_Type;
      Pubvalue_Id : Tkmrpc.Types.Blob_Id_Type);
   --  Sets the public value for the given KE context using parameters of the
   --  specified algorithm.

   procedure Generate
     (Id : Tkmrpc.Types.Ke_Id_Type);
   --  Generate the shared secret for the given KE context.

   procedure Generate
     (Ids : Tkmrpc.Types.Ke_Ids_Type);
   --  Generate the shared secret for all the given KE contexts.

   procedure Length
     (Ids       : Tkmrpc.Types.Ke_Ids_Type;
      Sk_0_Len  : out Tkmrpc.Types.Byte_Sequence_Range;
      Sk_1n_Len : out Tkmrpc.Types.Byte_Sequence_Range);
   --  Returns the length of the first and all following shared secrets of the
   --  given KE contexts.

   procedure Consume
     (Ids   :     Tkmrpc.Types.Ke_Ids_Type;
      Sk_0  : out Tkmrpc.Types.Byte_Sequence;
      Sk_1n : out Tkmrpc.Types.Byte_Sequence);
   --  Consume all the given KE contexts, returns the first shared secret and
   --  the rest (if any) in separate byte sequences.

   procedure Reset (Id : Tkmrpc.Types.Ke_Id_Type);
   --  Reset KE context with given id.

   function Validate_Ids
     (Ids : Tkmrpc.Types.Ke_Ids_Type)
      return Boolean;
   --  Check if the given KE context IDs are valid.

end Tkm.Servers.Ike.Ke;
