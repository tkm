--
--  Copyright (C) 2021  Tobias Brunner <tobias@codelabs.ch>
--  Copyright (C) 2013  Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2013  Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Interfaces;

with Tkmrpc.Contexts.ke;

with Tkm.Logger;
with Tkm.Crypto.Random;
with Tkm.Diffie_Hellman;
with Tkm.Servers.Ike.Blob;

package body Tkm.Servers.Ike.Ke
is

   package L renames Tkm.Logger;

   procedure Create
     (Id     :     Tkmrpc.Types.Ke_Id_Type;
      Kea_Id :     Tkmrpc.Types.Kea_Id_Type;
      Priv   : out Tkmrpc.Types.Ke_Priv_Type;
      Pub    : out Tkmrpc.Types.Ke_Pubvalue_Type);
   --  Create a key pair for the given KE algorithm

   -------------------------------------------------------------------------

   procedure Consume
     (Ids   :     Tkmrpc.Types.Ke_Ids_Type;
      Sk_0  : out Tkmrpc.Types.Byte_Sequence;
      Sk_1n : out Tkmrpc.Types.Byte_Sequence)
   is
      Ke_Id  : Tkmrpc.Types.Ke_Id_Type;
      Key    : Tkmrpc.Types.Ke_Key_Type;
      Offset : Tkmrpc.Types.Byte_Sequence_Range := 0;
   begin
      Ke_Id := Tkmrpc.Types.Ke_Id_Type (Ids.Data (Ids.Data'First));
      Tkmrpc.Contexts.ke.consume (Id         => Ke_Id,
                                  shared_key => Key);
      L.Log (Message => "KE context" & Ke_Id'Img & " consumed");
      Sk_0 (Sk_0'First .. Sk_0'First + Key.Size - 1)
        := Key.Data (Key.Data'First .. Key.Data'First + Key.Size - 1);

      for I in Ids.Data'First + 1 .. Ids.Data'First + Ids.Size - 1 loop
         Ke_Id := Tkmrpc.Types.Ke_Id_Type (Ids.Data (I));
         Tkmrpc.Contexts.ke.consume (Id         => Ke_Id,
                                     shared_key => Key);
         L.Log (Message => "KE context" & Ke_Id'Img & " consumed");
         Sk_1n (Sk_1n'First + Offset .. Sk_1n'First + Offset + Key.Size - 1)
           := Key.Data (Key.Data'First .. Key.Data'First + Key.Size - 1);
         Offset := Offset + Key.Size;
      end loop;
   end Consume;

   -------------------------------------------------------------------------

   procedure Create
     (Id     :     Tkmrpc.Types.Ke_Id_Type;
      Kea_Id :     Tkmrpc.Types.Kea_Id_Type;
      Priv   : out Tkmrpc.Types.Ke_Priv_Type;
      Pub    : out Tkmrpc.Types.Ke_Pubvalue_Type)
   is
      Group_Size   : constant Tkmrpc.Types.Byte_Sequence_Range
        := Diffie_Hellman.Get_Group_Size
          (Kea_Id => Tkmrpc.Types.Ke_Algorithm_Type (Kea_Id));
      Random_Chunk : Tkmrpc.Types.Byte_Sequence (1 .. Group_Size);
   begin
      L.Log (Message => "Creating DH key pair with algorithm" & Kea_Id'Img
             & ", KE context" & Id'Img);

      Priv      := Tkmrpc.Types.Null_Ke_Priv_Type;
      Priv.Size := Group_Size;
      Pub       := Tkmrpc.Types.Null_Ke_Pubvalue_Type;
      Pub.Size  := Group_Size;

      Random_Chunk := Crypto.Random.Get (Size => Random_Chunk'Length);

      Diffie_Hellman.Compute_Xa_Ya
        (Kea_Id       => Tkmrpc.Types.Ke_Algorithm_Type (Kea_Id),
         Random_Bytes => Random_Chunk,
         Xa           => Priv.Data (1 .. Group_Size),
         Ya           => Pub.Data (1 .. Group_Size));

      L.Log (Message => "DH key pair for KE context" & Id'Img & " created");
   end Create;

   -------------------------------------------------------------------------

   procedure Generate
     (Id : Tkmrpc.Types.Ke_Id_Type)
   is
   begin
      declare
         Kea_Id     : constant Tkmrpc.Types.Ke_Algorithm_Type
           := Tkmrpc.Types.Ke_Algorithm_Type
             (Tkmrpc.Contexts.ke.get_kea_id (Id => Id));
         Group_Size : constant Tkmrpc.Types.Byte_Sequence_Range
           := Diffie_Hellman.Get_Group_Size (Kea_Id => Kea_Id);
         Priv       : constant Tkmrpc.Types.Ke_Priv_Type
           := Tkmrpc.Contexts.ke.get_secvalue (Id => Id);
         Pub        : constant Tkmrpc.Types.Ke_Pubvalue_Type
           := Tkmrpc.Contexts.ke.get_pubvalue (Id => Id);
         Key        : Tkmrpc.Types.Ke_Key_Type
           := Tkmrpc.Types.Null_Ke_Key_Type;
      begin
         L.Log (Message => "Generating shared secret for KE context" & Id'Img);
         Diffie_Hellman.Compute_Zz
           (Kea_Id => Kea_Id,
            Xa     => Priv.Data (1 .. Priv.Size),
            Yb     => Pub.Data (1 .. Pub.Size),
            Zz     => Key.Data (1 .. Group_Size));
         Key.Size := Group_Size;
         Tkmrpc.Contexts.ke.generate (Id         => Id,
                                      shared_key => Key,
                                      timestamp  => 0);
      end;
   end Generate;

   -------------------------------------------------------------------------

   procedure Generate
     (Ids : Tkmrpc.Types.Ke_Ids_Type)
   is
   begin
      for I in Ids.Data'First .. Ids.Data'First + Ids.Size - 1 loop
         Generate (Id => Tkmrpc.Types.Ke_Id_Type (Ids.Data (I)));
      end loop;
   end Generate;

   -------------------------------------------------------------------------

   function Get
     (Id          : Tkmrpc.Types.Ke_Id_Type;
      Kea_Id      : Tkmrpc.Types.Kea_Id_Type;
      Pubvalue_Id : Tkmrpc.Types.Blob_Id_Type)
      return Tkmrpc.Types.Blob_Length_Type
   is
      use type Tkmrpc.Contexts.ke.ke_State_Type;

      Priv : Tkmrpc.Types.Ke_Priv_Type;
      Pub  : Tkmrpc.Types.Ke_Pubvalue_Type;
   begin
      Create
           (Id     => Id,
            Kea_Id => Kea_Id,
            Priv   => Priv,
            Pub    => Pub);
      if Tkmrpc.Contexts.ke.Get_State (Id => Id) = Tkmrpc.Contexts.ke.clean
      then
         Tkmrpc.Contexts.ke.initiate (Id       => Id,
                                      kea_id   => Kea_Id,
                                      secvalue => Priv);
      else
         Tkmrpc.Contexts.ke.set_priv (Id       => Id,
                                      secvalue => Priv);
      end if;

      Blob.Create
        (Id     => Pubvalue_Id,
         Length => Tkmrpc.Types.Blob_Length_Type (Pub.Size));

      Blob.Write
        (Id     => Pubvalue_Id,
         Offset => 0,
         Data   => Pub.Data (Pub.Data'First .. Pub.Data'First + Pub.Size - 1));

      return Tkmrpc.Types.Blob_Length_Type (Pub.Size);
   end Get;

   -------------------------------------------------------------------------

   procedure Length
     (Ids       :     Tkmrpc.Types.Ke_Ids_Type;
      Sk_0_Len  : out Tkmrpc.Types.Byte_Sequence_Range;
      Sk_1n_Len : out Tkmrpc.Types.Byte_Sequence_Range)
   is
      Len    : Tkmrpc.Types.Byte_Sequence_Range := 0;
      Kea_Id : Tkmrpc.Types.Kea_Id_Type;
   begin
      Kea_Id := Tkmrpc.Contexts.ke.get_kea_id
        (Id => Tkmrpc.Types.Ke_Id_Type (Ids.Data (Ids.Data'First)));
      Sk_0_Len := Diffie_Hellman.Get_Group_Size
        (Kea_Id => Tkmrpc.Types.Ke_Algorithm_Type (Kea_Id));

      for I in Ids.Data'First + 1 .. Ids.Data'First + Ids.Size - 1 loop
         Kea_Id := Tkmrpc.Contexts.ke.get_kea_id
           (Id => Tkmrpc.Types.Ke_Id_Type (Ids.Data (I)));
         Len := Len + Diffie_Hellman.Get_Group_Size
           (Kea_Id => Tkmrpc.Types.Ke_Algorithm_Type (Kea_Id));
      end loop;
      Sk_1n_Len := Len;
   end Length;

   -------------------------------------------------------------------------

   procedure Reset (Id : Tkmrpc.Types.Ke_Id_Type)
   is
   begin
      L.Log (Message => "Resetting KE context" & Id'Img);
      Tkmrpc.Contexts.ke.reset (Id => Id);
   end Reset;

   -------------------------------------------------------------------------

   procedure Set
     (Id          : Tkmrpc.Types.Ke_Id_Type;
      Kea_Id      : Tkmrpc.Types.Kea_Id_Type;
      Pubvalue_Id : Tkmrpc.Types.Blob_Id_Type)
   is
      use type Tkmrpc.Contexts.ke.ke_State_Type;

      Pub : Tkmrpc.Types.Ke_Pubvalue_Type;
   begin
      Pub.Size := Tkmrpc.Types.Ke_Pubvalue_Type_Range
        (Blob.Get_Length (Id => Pubvalue_Id));

      Blob.Read
        (Id     => Pubvalue_Id,
         Offset => 0,
         Length => Tkmrpc.Types.Blob_Length_Type (Pub.Size),
         Data   => Pub.Data (Pub.Data'First .. Pub.Data'First + Pub.Size - 1));
      Blob.Reset (Id => Pubvalue_Id);

      if Tkmrpc.Contexts.ke.Get_State (Id => Id) = Tkmrpc.Contexts.ke.clean
      then
         Tkmrpc.Contexts.ke.respond (Id       => Id,
                                     kea_id   => Kea_Id,
                                     pubvalue => Pub);
      else
         Tkmrpc.Contexts.ke.set_pubvalue (Id       => Id,
                                          pubvalue => Pub);
      end if;
   end Set;

   -------------------------------------------------------------------------

   function Validate_Ids
     (Ids : Tkmrpc.Types.Ke_Ids_Type)
      return Boolean
   is
   begin
      return (for all I in Ids.Data'First .. Ids.Data'First + Ids.Size - 1 =>
                Ids.Data (I) in
                Interfaces.Unsigned_64 (Tkmrpc.Types.Ke_Id_Type'First) ..
                Interfaces.Unsigned_64 (Tkmrpc.Types.Ke_Id_Type'Last));
   end Validate_Ids;

end Tkm.Servers.Ike.Ke;
