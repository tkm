--
--  Copyright (C) 2020  Tobias Brunner <tobias@codelabs.ch>
--  Copyright (C) 2013  Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2013  Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Interfaces;

with X509.Keys;
with X509.Certs;

with Tkmrpc.Contexts.ke;
with Tkmrpc.Contexts.nc;
with Tkmrpc.Contexts.isa;
with Tkmrpc.Contexts.ae;
with Tkmrpc.Contexts.cc;

with Tkm.Config;
with Tkm.Utils;
with Tkm.Logger;
with Tkm.Identities;
with Tkm.Servers.Ike.Blob;
with Tkm.Servers.Ike.Ke;
with Tkm.Key_Derivation;
with Tkm.Private_Key;
with Tkm.Crypto.Aead;
with Tkm.Crypto.Hmac_Sha512;
with Tkm.Crypto.Rsa_Pkcs1_Sha1;

package body Tkm.Servers.Ike.Isa
is

   package L renames Tkm.Logger;

   function Compute_Auth_Octets
     (Ae_Id        : Tkmrpc.Types.Ae_Id_Type;
      Init_Message : Tkmrpc.Types.Init_Message_Type;
      Idx          : Tkmrpc.Types.Identity_Type;
      Verify       : Boolean)
      return Tkmrpc.Types.Byte_Sequence;
   --  Compute local/remote AUTH octets for given AE context depending on the
   --  verify flag: Verify = False => local.

   function Compute_IntAuth
     (Ae_Id        : Tkmrpc.Types.Ae_Id_Type)
      return Tkmrpc.Types.Byte_Sequence;
   --  Compute IntAuth for given AE context.

   procedure Derive_Child_Keys
     (Old_Sk_D  :        Tkmrpc.Types.Key_Type;
      Sk_0      :        Tkmrpc.Types.Byte_Sequence;
      Sk_1n     :        Tkmrpc.Types.Byte_Sequence;
      Initiator :        Tkmrpc.Types.Init_Type;
      Nonce_Loc :        Tkmrpc.Types.Nonce_Type;
      Nonce_Rem :        Tkmrpc.Types.Nonce_Type;
      Spi_Loc   :        Tkmrpc.Types.Ike_Spi_Type;
      Spi_Rem   :        Tkmrpc.Types.Ike_Spi_Type;
      Sk_D      : in out Tkmrpc.Types.Key_Type;
      Sk_Ai     : in out Tkmrpc.Types.Key_Type;
      Sk_Ar     : in out Tkmrpc.Types.Key_Type;
      Sk_Ei     : in out Tkmrpc.Types.Key_Type;
      Sk_Er     : in out Tkmrpc.Types.Key_Type;
      Sk_Pi     : in out Tkmrpc.Types.Key_Type;
      Sk_Pr     : in out Tkmrpc.Types.Key_Type);
   --  Derive keys for an SA derived from a previous SA (either rekeying or
   --  IKE_INTERMEDIATE)

   -------------------------------------------------------------------------

   procedure Auth
     (Isa_Id       : Tkmrpc.Types.Isa_Id_Type;
      Cc_Id        : Tkmrpc.Types.Cc_Id_Type;
      Init_Message : Tkmrpc.Types.Init_Message_Type;
      Signature    : Tkmrpc.Types.Signature_Type)
   is
      package RSA renames Crypto.Rsa_Pkcs1_Sha1;

      Raw_Cert  : constant Tkmrpc.Types.Certificate_Type
        := Tkmrpc.Contexts.cc.get_certificate (Id => Cc_Id);
      User_Cert : X509.Certs.Certificate_Type;
   begin
      X509.Certs.Load
        (Buffer => Utils.To_X509_Bytes
           (Item => Raw_Cert.Data (Raw_Cert.Data'First .. Raw_Cert.Size)),
         Cert   => User_Cert);
      L.Log (Message => "Verifying remote signature for ISA context"
             & Isa_Id'Img & " with CC context" & Cc_Id'Img);

      declare
         Pubkey   : constant X509.Keys.RSA_Public_Key_Type
           := X509.Certs.Get_Public_Key (Cert => User_Cert);
         Verifier : RSA.Verifier_Type;
         Ae_Id    : constant Tkmrpc.Types.Ae_Id_Type
           := Tkmrpc.Contexts.isa.get_ae_id (Id => Isa_Id);
         Ri_Id    : constant Tkmrpc.Types.Ri_Id_Type
           := Tkmrpc.Contexts.cc.get_remote_id (Id => Cc_Id);

         --  The ri_id is equal to the policy id for now and can be used to get
         --  the ri from the corresponding policy. This must be adjusted once
         --  IKE processes TKM-specific config data and thus supports fully
         --  fledged remote identity handling.

         Octets   : constant Tkmrpc.Types.Byte_Sequence
           := Compute_Auth_Octets
             (Ae_Id        => Ae_Id,
              Init_Message => Init_Message,
              Idx          => Config.Get_Policy
                (Id => Tkmrpc.Types.Sp_Id_Type (Ri_Id)).Remote_Identity,
              Verify       => True);
      begin
         RSA.Init (Ctx => Verifier,
                   N   => X509.Keys.Get_Modulus (Key => Pubkey),
                   E   => X509.Keys.Get_Pub_Exponent (Key => Pubkey));

         if not RSA.Verify
           (Ctx       => Verifier,
            Data      => Octets,
            Signature => Signature.Data
              (Signature.Data'First .. Signature.Size))
         then
            raise Authentication_Failure with "Authentication failed for ISA"
              & " context" & Isa_Id'Img;
         end if;

         Tkmrpc.Contexts.ae.authenticate
           (Id         => Tkmrpc.Contexts.isa.get_ae_id (Id => Isa_Id),
            ca_context => 1,
            authag_id  => 1,
            ri_id      => Ri_Id,
            not_before => 1,
            not_after  => 1);
         L.Log (Message => "Authentication of ISA context" & Isa_Id'Img
                & " successful");
      end;
   end Auth;

   -------------------------------------------------------------------------

   function Compute_Auth_Octets
     (Ae_Id        : Tkmrpc.Types.Ae_Id_Type;
      Init_Message : Tkmrpc.Types.Init_Message_Type;
      Idx          : Tkmrpc.Types.Identity_Type;
      Verify       : Boolean)
      return Tkmrpc.Types.Byte_Sequence
   is
      IntAuth : constant Tkmrpc.Types.Byte_Sequence
        := Compute_IntAuth (Ae_Id => Ae_Id);

      Sk_P  : Tkmrpc.Types.Key_Type;
      Nonce : Tkmrpc.Types.Nonce_Type;
      Prf   : Crypto.Hmac_Sha512.Context_Type;
   begin
      if Verify then
         L.Log (Message => "Generating remote AUTH octets");
         Sk_P  := Tkmrpc.Contexts.ae.get_sk_ike_auth_rem (Id => Ae_Id);
         Nonce := Tkmrpc.Contexts.ae.get_nonce_loc (Id => Ae_Id);
      else
         L.Log (Message => "Generating local AUTH octets");
         Sk_P  := Tkmrpc.Contexts.ae.get_sk_ike_auth_loc (Id => Ae_Id);
         Nonce := Tkmrpc.Contexts.ae.get_nonce_rem (Id => Ae_Id);
      end if;

      Crypto.Hmac_Sha512.Init
        (Ctx => Prf,
         Key => Sk_P.Data
           (Sk_P.Data'First .. Sk_P.Size));

      declare
         Octets     : Tkmrpc.Types.Byte_Sequence
           (1 .. Init_Message.Size + Nonce.Size +
              Crypto.Hmac_Sha512.Hash_Output_Length + IntAuth'Length);
         Encoded_Id : constant Tkmrpc.Types.Identity_Type
           := Tkm.Identities.Encode (Identity => Idx);
         Idx_Nc     : constant Positive := Octets'First + Init_Message.Size;
         Idx_Sk     : constant Positive := Idx_Nc + Nonce.Size;
         Idx_Ia     : constant Positive
           := Idx_Sk + Crypto.Hmac_Sha512.Hash_Output_Length;
      begin

         --  Octets = Init_Message | nonce | prf(Sk_p, Idx) [ | IntAuth ]

         Octets (1 .. Idx_Nc - 1)
           := Init_Message.Data (Init_Message.Data'First .. Init_Message.Size);
         Octets (Idx_Nc .. Idx_Sk - 1)
           := Nonce.Data (Nonce.Data'First .. Nonce.Size);
         Octets (Idx_Sk .. Idx_Ia - 1)
           := Crypto.Hmac_Sha512.Generate
             (Ctx  => Prf,
              Data => Encoded_Id.Data
                (Encoded_Id.Data'First .. Encoded_Id.Size));
         Octets (Idx_Ia .. Octets'Last) := IntAuth;

         L.Log (Message => "AUTH Octets " & Utils.To_Hex_String
                (Input => Octets));
         return Octets;
      end;
   end Compute_Auth_Octets;

   -------------------------------------------------------------------------

   function Compute_IntAuth
     (Ae_Id        : Tkmrpc.Types.Ae_Id_Type)
      return Tkmrpc.Types.Byte_Sequence
   is
      use type Tkmrpc.Types.Init_Type;
      use type Tkmrpc.Types.Intauth_Count_Type;

      Initiator   : constant Tkmrpc.Types.Init_Type
        := Tkmrpc.Contexts.ae.is_initiator (Id => Ae_Id);
      Count       : constant Tkmrpc.Types.Intauth_Count_Type
        := Tkmrpc.Contexts.ae.get_intauth_count (Id => Ae_Id);
      IntAuth_Loc : constant Tkmrpc.Types.Intauth_Type
        := Tkmrpc.Contexts.ae.get_intauth_loc (Id => Ae_Id);
      IntAuth_Rem : constant Tkmrpc.Types.Intauth_Type
        := Tkmrpc.Contexts.ae.get_intauth_rem (Id => Ae_Id);
      IntAuth_Len : constant Natural
        := (if IntAuth_Loc.Size > 1 then IntAuth_Loc.Size else 0);
   begin
      if Count = 0 then
         return Tkm.Null_Byte_Sequence;
      elsif Count rem 2 /= 0 then
         raise Authentication_Failure with "Number of IntAuth calculations"
           & " invalid for AE context" & Ae_Id'Img;
      end if;

      declare
         Mid     : constant Tkmrpc.Types.Byte_Sequence
           := Tkm.Utils.To_Bytes
             (Input => Tkm.Utils.Host_To_Network
                  (Input => Interfaces.Unsigned_32 (1 + Count / 2)));
         Octets  : Tkmrpc.Types.Byte_Sequence
           (1 .. 2 * IntAuth_Len + Mid'Length);
         Idx_Iar : constant Positive := Octets'First + IntAuth_Len;
         Idx_Mid : constant Positive := Idx_Iar + IntAuth_Len;
      begin

         --  Octets = IntAuth_iN | IntAuth_rN | IKE_AUTH_MID

         if Initiator = 1 then
            Octets (1 .. Idx_Iar - 1)
              := IntAuth_Loc.Data
                (IntAuth_Loc.Data'First ..
                   IntAuth_Loc.Data'First + IntAuth_Len - 1);
            Octets (Idx_Iar .. Idx_Mid - 1)
              := IntAuth_Rem.Data
                (IntAuth_Rem.Data'First ..
                   IntAuth_Rem.Data'First + IntAuth_Len - 1);
         else
            Octets (1 .. Idx_Iar - 1)
              := IntAuth_Rem.Data
                (IntAuth_Rem.Data'First ..
                   IntAuth_Rem.Data'First + IntAuth_Len - 1);
            Octets (Idx_Iar .. Idx_Mid - 1)
              := IntAuth_Loc.Data
                (IntAuth_Loc.Data'First ..
                   IntAuth_Loc.Data'First + IntAuth_Len - 1);
         end if;
         Octets (Idx_Mid .. Octets'Last) := Mid;
         return Octets;
      end;
   end Compute_IntAuth;

   -------------------------------------------------------------------------

   procedure Create
     (Isa_Id    :     Tkmrpc.Types.Isa_Id_Type;
      Ae_Id     :     Tkmrpc.Types.Ae_Id_Type;
      Ia_Id     :     Tkmrpc.Types.Ia_Id_Type;
      Ke_Id     :     Tkmrpc.Types.Ke_Id_Type;
      Nc_Loc_Id :     Tkmrpc.Types.Nc_Id_Type;
      Nonce_Rem :     Tkmrpc.Types.Nonce_Type;
      Initiator :     Tkmrpc.Types.Init_Type;
      Spi_Loc   :     Tkmrpc.Types.Ike_Spi_Type;
      Spi_Rem   :     Tkmrpc.Types.Ike_Spi_Type;
      Block_Len : out Tkmrpc.Types.Block_Len_Type;
      Icv_Len   : out Tkmrpc.Types.Icv_Len_Type;
      Iv_Len    : out Tkmrpc.Types.Iv_Len_Type)
   is
      Secret    : Tkmrpc.Types.Ke_Key_Type;
      Nonce_Loc : Tkmrpc.Types.Nonce_Type;

      Sk_Ai, Sk_Ar       : Tkmrpc.Types.Key_Type
        := (Size => Crypto.Aead.Int_Key_Length,
            Data => (others => 0));
      Sk_Ei, Sk_Er       : Tkmrpc.Types.Key_Type
        := (Size => Crypto.Aead.Enc_Key_Length,
            Data => (others => 0));
      Sk_D, Sk_Pi, Sk_Pr : Tkmrpc.Types.Key_Type
        := (Size => Crypto.Hmac_Sha512.Hash_Output_Length,
            Data => (others => 0));
   begin
      Block_Len := Crypto.Aead.Block_Length;
      Icv_Len   := Crypto.Aead.Icv_Length;
      Iv_Len    := Crypto.Aead.Iv_Length;

      L.Log (Message => "Creating new ISA context with ID" & Isa_Id'Img
             & " (KE" & Ke_Id'Img & ", nonce" & Nc_Loc_Id'Img
             & ", spi_loc " & Utils.To_Hex_String
               (Input => Interfaces.Unsigned_64 (Spi_Loc))
             & ", spi_rem " & Utils.To_Hex_String
               (Input => Interfaces.Unsigned_64 (Spi_Rem)) & ")");

      Tkm.Servers.Ike.Ke.Generate (Id => Ke_Id);

      Tkmrpc.Contexts.ke.consume (Id         => Ke_Id,
                                  shared_key => Secret);
      L.Log (Message => "KE context" & Ke_Id'Img & " consumed");

      Tkmrpc.Contexts.nc.consume (Id    => Nc_Loc_Id,
                                  nonce => Nonce_Loc);
      L.Log (Message => "Nonce context" & Nc_Loc_Id'Img & " consumed");

      --  Use PRF-HMAC-SHA512 for now.

      declare
         use type Tkmrpc.Types.Init_Type;

         Prf           : Crypto.Hmac_Sha512.Context_Type;
         Skeyseed      : Tkmrpc.Types.Byte_Sequence
           (1 .. Crypto.Hmac_Sha512.Hash_Output_Length);
         Fixed_Nonce   : Tkmrpc.Types.Byte_Sequence
           (1 .. Nonce_Rem.Size + Nonce_Loc.Size);
         Seed_Size     : constant Positive := Nonce_Rem.Size + Nonce_Loc.Size
           + 16;
         Prf_Plus_Seed : Tkmrpc.Types.Byte_Sequence (1 .. Seed_Size);
      begin
         if Initiator = 1 then
            Fixed_Nonce (Fixed_Nonce'First .. Nonce_Loc.Size)
              := Nonce_Loc.Data (Nonce_Loc.Data'First .. Nonce_Loc.Size);
            Fixed_Nonce (Nonce_Loc.Size + 1 .. Fixed_Nonce'Last)
              := Nonce_Rem.Data (Nonce_Rem.Data'First .. Nonce_Rem.Size);
            Prf_Plus_Seed (1 .. Nonce_Rem.Size + Nonce_Loc.Size)
              := Fixed_Nonce;
            Prf_Plus_Seed
              (Nonce_Rem.Size + Nonce_Loc.Size + 1 .. Nonce_Rem.Size
               + Nonce_Loc.Size + 8)
              := Utils.To_Bytes (Input => Interfaces.Unsigned_64 (Spi_Loc));
            Prf_Plus_Seed
              (Nonce_Rem.Size + Nonce_Loc.Size + 9 ..
                 Prf_Plus_Seed'Last)
                := Utils.To_Bytes (Input => Interfaces.Unsigned_64 (Spi_Rem));
         else
            Fixed_Nonce (Fixed_Nonce'First .. Nonce_Rem.Size)
              := Nonce_Rem.Data (Nonce_Rem.Data'First .. Nonce_Rem.Size);
            Fixed_Nonce (Nonce_Rem.Size + 1 .. Fixed_Nonce'Last)
              := Nonce_Loc.Data (Nonce_Loc.Data'First .. Nonce_Loc.Size);
            Prf_Plus_Seed (1 .. Nonce_Rem.Size + Nonce_Loc.Size)
              := Fixed_Nonce;
            Prf_Plus_Seed
              (Nonce_Rem.Size + Nonce_Loc.Size + 1 .. Nonce_Rem.Size
               + Nonce_Loc.Size + 8)
              := Utils.To_Bytes (Input => Interfaces.Unsigned_64 (Spi_Rem));
            Prf_Plus_Seed
              (Nonce_Rem.Size + Nonce_Loc.Size + 9 ..
                 Prf_Plus_Seed'Last)
                := Utils.To_Bytes (Input => Interfaces.Unsigned_64 (Spi_Loc));
         end if;

         --  SKEYSEED    = prf (Ni | Nr, Shared_Secret)
         --  PRFPLUSSEED = Ni | Nr | SPIi | SPIr

         Crypto.Hmac_Sha512.Init (Ctx => Prf,
                                  Key => Fixed_Nonce);
         Skeyseed := Crypto.Hmac_Sha512.Generate
           (Ctx  => Prf,
            Data => Secret.Data
              (Secret.Data'First .. Secret.Data'First + Secret.Size - 1));

         Key_Derivation.Derive_Ike_Keys (Skeyseed => Skeyseed,
                                         Prf_Seed => Prf_Plus_Seed,
                                         Sk_D     => Sk_D,
                                         Sk_Ai    => Sk_Ai,
                                         Sk_Ar    => Sk_Ar,
                                         Sk_Ei    => Sk_Ei,
                                         Sk_Er    => Sk_Er,
                                         Sk_Pi    => Sk_Pi,
                                         Sk_Pr    => Sk_Pr);

         --  Create ae, isa and AEAD contexts

         L.Log (Message => "Creating new AE context with ID" & Ae_Id'Img);
         Tkmrpc.Contexts.ae.create
           (Id              => Ae_Id,
            iag_id          => 1,
            keag_id         => 1,
            creation_time   => 0,
            initiator       => Initiator,
            sk_ike_auth_loc => (if Initiator = 1 then Sk_Pi else Sk_Pr),
            sk_ike_auth_rem => (if Initiator = 0 then Sk_Pi else Sk_Pr),
            nonce_loc       => Nonce_Loc,
            nonce_rem       => Nonce_Rem,
            spi_loc         => Spi_Loc,
            spi_rem         => Spi_Rem);
         Tkmrpc.Contexts.isa.create
           (Id            => Isa_Id,
            ae_id         => Ae_Id,
            ia_id         => Ia_Id,
            sk_d          => Sk_D,
            creation_time => 0);
         Crypto.Aead.Create
           (Isa_Id   => Isa_Id,
            Sk_A_In  => (if Initiator = 0 then Sk_Ai else Sk_Ar),
            Sk_A_Out => (if Initiator = 1 then Sk_Ai else Sk_Ar),
            Sk_E_In  => (if Initiator = 0 then Sk_Ei else Sk_Er),
            Sk_E_Out => (if Initiator = 1 then Sk_Ei else Sk_Er));
      end;
   end Create;

   -------------------------------------------------------------------------

   procedure Create_Child
     (Isa_Id        :     Tkmrpc.Types.Isa_Id_Type;
      Parent_Isa_Id :     Tkmrpc.Types.Isa_Id_Type;
      Ia_Id         :     Tkmrpc.Types.Ia_Id_Type;
      Ke_Ids        :     Tkmrpc.Types.Ke_Ids_Type;
      Nc_Loc_Id     :     Tkmrpc.Types.Nc_Id_Type;
      Nonce_Rem     :     Tkmrpc.Types.Nonce_Type;
      Initiator     :     Tkmrpc.Types.Init_Type;
      Spi_Loc       :     Tkmrpc.Types.Ike_Spi_Type;
      Spi_Rem       :     Tkmrpc.Types.Ike_Spi_Type;
      Block_Len     : out Tkmrpc.Types.Block_Len_Type;
      Icv_Len       : out Tkmrpc.Types.Icv_Len_Type;
      Iv_Len        : out Tkmrpc.Types.Iv_Len_Type)
   is
      use type Tkmrpc.Types.Init_Type;

      Ae_Id     : constant Tkmrpc.Types.Ae_Id_Type
        := Tkmrpc.Contexts.isa.get_ae_id (Id => Parent_Isa_Id);
      Old_Sk_D  : constant Tkmrpc.Types.Key_Type
        := Tkmrpc.Contexts.isa.get_sk_d (Id => Parent_Isa_Id);

      Ke_Id     : Tkmrpc.Types.Ke_Id_Type;
      Nonce_Loc : Tkmrpc.Types.Nonce_Type;
      Sk_0_Len  : Tkmrpc.Types.Byte_Sequence_Range;
      Sk_1n_Len : Tkmrpc.Types.Byte_Sequence_Range;
      Sk_D      : Tkmrpc.Types.Key_Type
        := (Size => Crypto.Hmac_Sha512.Hash_Output_Length,
            Data => (others => 0));

      Sk_Ai, Sk_Ar : Tkmrpc.Types.Key_Type
        := (Size => Crypto.Aead.Int_Key_Length,
            Data => (others => 0));
      Sk_Ei, Sk_Er : Tkmrpc.Types.Key_Type
        := (Size => Crypto.Aead.Enc_Key_Length,
            Data => (others => 0));
      Sk_Pi, Sk_Pr : Tkmrpc.Types.Key_Type := Tkmrpc.Types.Null_Key_Type;
   begin
      Block_Len := Crypto.Aead.Block_Length;
      Icv_Len   := Crypto.Aead.Icv_Length;
      Iv_Len    := Crypto.Aead.Iv_Length;

      if not Tkm.Servers.Ike.Ke.Validate_Ids (Ids => Ke_Ids)
      then
         raise Invalid_Key_Exchange with "Invalid KE IDs";
      end if;

      Ke_Id := Tkmrpc.Types.Ke_Id_Type (Ke_Ids.Data (Ke_Ids.Data'First));

      L.Log (Message => "Creating new child ISA context with ID" & Isa_Id'Img
             & " (Parent Isa" & Parent_Isa_Id'Img & ", KE" & Ke_Id'Img
             & " #1 /" & Ke_Ids.Size'Img
             & ", nonce" & Nc_Loc_Id'Img
             & ", spi_loc " & Utils.To_Hex_String
               (Input => Interfaces.Unsigned_64 (Spi_Loc))
             & ", spi_rem " & Utils.To_Hex_String
               (Input => Interfaces.Unsigned_64 (Spi_Rem)) & ")");

      Tkmrpc.Contexts.nc.consume (Id    => Nc_Loc_Id,
                                  nonce => Nonce_Loc);
      L.Log (Message => "Nonce context" & Nc_Loc_Id'Img & " consumed");

      Tkm.Servers.Ike.Ke.Length
        (Ids => Ke_Ids,
         Sk_0_Len  => Sk_0_Len,
         Sk_1n_Len => Sk_1n_Len);

      declare
         Sk_0  : Tkmrpc.Types.Byte_Sequence (1 .. Sk_0_Len);
         Sk_1n : Tkmrpc.Types.Byte_Sequence (1 .. Sk_1n_Len);
      begin
         Tkm.Servers.Ike.Ke.Generate (Ids => Ke_Ids);
         Tkm.Servers.Ike.Ke.Consume
           (Ids   => Ke_Ids,
            Sk_0  => Sk_0,
            Sk_1n => Sk_1n);

         Derive_Child_Keys
           (Old_Sk_D  => Old_Sk_D,
            Sk_0      => Sk_0,
            Sk_1n     => Sk_1n,
            Initiator => Initiator,
            Nonce_Loc => Nonce_Loc,
            Nonce_Rem => Nonce_Rem,
            Spi_Loc   => Spi_Loc,
            Spi_Rem   => Spi_Rem,
            Sk_D      => Sk_D,
            Sk_Ai     => Sk_Ai,
            Sk_Ar     => Sk_Ar,
            Sk_Ei     => Sk_Ei,
            Sk_Er     => Sk_Er,
            Sk_Pi     => Sk_Pi,
            Sk_Pr     => Sk_Pr);
      end;

      --  Create isa and AEAD contexts

      L.Log (Message => "Creating new ISA context with ID" & Isa_Id'Img);
      Tkmrpc.Contexts.isa.create
        (Id            => Isa_Id,
         ae_id         => Ae_Id,
         ia_id         => Ia_Id,
         sk_d          => Sk_D,
         creation_time => 0);
      Crypto.Aead.Create
        (Isa_Id   => Isa_Id,
         Sk_A_In  => (if Initiator = 0 then Sk_Ai else Sk_Ar),
         Sk_A_Out => (if Initiator = 1 then Sk_Ai else Sk_Ar),
         Sk_E_In  => (if Initiator = 0 then Sk_Ei else Sk_Er),
         Sk_E_Out => (if Initiator = 1 then Sk_Ei else Sk_Er));
   end Create_Child;

   -------------------------------------------------------------------------

   procedure Derive_Child_Keys
     (Old_Sk_D  :        Tkmrpc.Types.Key_Type;
      Sk_0      :        Tkmrpc.Types.Byte_Sequence;
      Sk_1n     :        Tkmrpc.Types.Byte_Sequence;
      Initiator :        Tkmrpc.Types.Init_Type;
      Nonce_Loc :        Tkmrpc.Types.Nonce_Type;
      Nonce_Rem :        Tkmrpc.Types.Nonce_Type;
      Spi_Loc   :        Tkmrpc.Types.Ike_Spi_Type;
      Spi_Rem   :        Tkmrpc.Types.Ike_Spi_Type;
      Sk_D      : in out Tkmrpc.Types.Key_Type;
      Sk_Ai     : in out Tkmrpc.Types.Key_Type;
      Sk_Ar     : in out Tkmrpc.Types.Key_Type;
      Sk_Ei     : in out Tkmrpc.Types.Key_Type;
      Sk_Er     : in out Tkmrpc.Types.Key_Type;
      Sk_Pi     : in out Tkmrpc.Types.Key_Type;
      Sk_Pr     : in out Tkmrpc.Types.Key_Type)
   is
      use type Tkmrpc.Types.Init_Type;

      --  Use PRF-HMAC-SHA512 for now.
      Prf           : Crypto.Hmac_Sha512.Context_Type;
      Skeyseed      : Tkmrpc.Types.Byte_Sequence
        (1 .. Crypto.Hmac_Sha512.Hash_Output_Length);
      Sk_Seed       : Tkmrpc.Types.Byte_Sequence
        (1 .. Sk_0'Length + Nonce_Loc.Size + Nonce_Rem.Size + Sk_1n'Length);
      Seed_Size     : constant Tkmrpc.Types.Byte_Sequence_Range
        := Nonce_Rem.Size + Nonce_Loc.Size + 16;
      Prf_Plus_Seed : Tkmrpc.Types.Byte_Sequence (1 .. Seed_Size);
      PPS_Idx1      : constant Tkmrpc.Types.Byte_Sequence_Range
        := Nonce_Loc.Size + Nonce_Rem.Size + 1;
      --  PRFPLUSSEED index of SPIi start
      PPS_Idx2      : constant Tkmrpc.Types.Byte_Sequence_Range
        := PPS_Idx1 + 8;
      --  PRFPLUSSEED index of SPIr start
      Sks_Idx1      : constant Tkmrpc.Types.Byte_Sequence_Range
        := Sk_0'Length + 1;
      --  SKEYSEED index of Ni start
      Sks_Idx2      : Tkmrpc.Types.Byte_Sequence_Range;
      --  SKEYSEED index of Nr start
      Sks_Idx3      : constant Tkmrpc.Types.Byte_Sequence_Range
        := Sk_0'Length + Nonce_Loc.Size + Nonce_Rem.Size + 1;
      --  SKEYSEED index of SK(1) start
   begin

      --  SKEYSEED    = prf (SK_d (old), SK(0) | Ni | Nr | SK(1) | ... SK(n))
      --  PRFPLUSSEED = Ni | Nr | SPIi | SPIr

      Sk_Seed (Sk_Seed'First .. Sk_0'Length)
        := Sk_0 (Sk_0'First .. Sk_0'First + Sk_0'Length - 1);
      if Initiator = 1 then
         Sks_Idx2 := Sks_Idx1 + Nonce_Loc.Size;
         Sk_Seed (Sks_Idx1 .. Sks_Idx2 - 1)
           := Nonce_Loc.Data  (Nonce_Loc.Data'First .. Nonce_Loc.Size);
         Sk_Seed (Sks_Idx2 .. Sks_Idx3 - 1)
           := Nonce_Rem.Data  (Nonce_Rem.Data'First .. Nonce_Rem.Size);
         Prf_Plus_Seed (Prf_Plus_Seed'First .. PPS_Idx1 - 1)
           := Sk_Seed (Sks_Idx1 .. Sks_Idx3 - 1);
         Prf_Plus_Seed (PPS_Idx1 .. PPS_Idx2 - 1)
           := Utils.To_Bytes (Input => Interfaces.Unsigned_64 (Spi_Loc));
         Prf_Plus_Seed (PPS_Idx2 .. Prf_Plus_Seed'Last)
           := Utils.To_Bytes (Input => Interfaces.Unsigned_64 (Spi_Rem));
      else
         Sks_Idx2 := Sks_Idx1 + Nonce_Rem.Size;
         Sk_Seed (Sks_Idx1 .. Sks_Idx2 - 1)
           := Nonce_Rem.Data  (Nonce_Rem.Data'First .. Nonce_Rem.Size);
         Sk_Seed (Sks_Idx2 .. Sks_Idx3 - 1)
           := Nonce_Loc.Data  (Nonce_Loc.Data'First .. Nonce_Loc.Size);
         Prf_Plus_Seed (Prf_Plus_Seed'First .. PPS_Idx1 - 1)
           := Sk_Seed (Sks_Idx1 .. Sks_Idx3 - 1);
         Prf_Plus_Seed (PPS_Idx1 .. PPS_Idx2 - 1)
           := Utils.To_Bytes (Input => Interfaces.Unsigned_64 (Spi_Rem));
         Prf_Plus_Seed (PPS_Idx2 .. Prf_Plus_Seed'Last)
           := Utils.To_Bytes (Input => Interfaces.Unsigned_64 (Spi_Loc));
      end if;
      Sk_Seed (Sks_Idx3 .. Sk_Seed'Last)
        := Sk_1n (Sk_1n'First .. Sk_1n'First + Sk_1n'Length - 1);

      Crypto.Hmac_Sha512.Init (Ctx => Prf,
                               Key => Old_Sk_D.Data (1 .. Old_Sk_D.Size));
      Skeyseed := Crypto.Hmac_Sha512.Generate (Ctx  => Prf,
                                               Data => Sk_Seed);
      Key_Derivation.Derive_Ike_Keys (Skeyseed => Skeyseed,
                                      Prf_Seed => Prf_Plus_Seed,
                                      Sk_D     => Sk_D,
                                      Sk_Ai    => Sk_Ai,
                                      Sk_Ar    => Sk_Ar,
                                      Sk_Ei    => Sk_Ei,
                                      Sk_Er    => Sk_Er,
                                      Sk_Pi    => Sk_Pi,
                                      Sk_Pr    => Sk_Pr);
   end Derive_Child_Keys;

   -------------------------------------------------------------------------

   procedure Int_Auth
     (Isa_Id  : Tkmrpc.Types.Isa_Id_Type;
      Inbound : Tkmrpc.Types.Inbound_Flag_Type;
      Data_Id : Tkmrpc.Types.Blob_Id_Type)
   is
      use type Tkmrpc.Types.Inbound_Flag_Type;
      use type Tkmrpc.Types.Intauth_Count_Type;

      Ae_Id : constant Tkmrpc.Types.Ae_Id_Type
        := Tkmrpc.Contexts.isa.get_ae_id (Id => Isa_Id);

      Sk_P    : Tkmrpc.Types.Key_Type;
      Data    : Tkmrpc.Types.Byte_Sequence
        (1 .. Natural (Blob.Get_Length (Id => Data_Id)));
      IntAuth : Tkmrpc.Types.Intauth_Type;
   begin
      if Inbound = 1 then
         L.Log (Message => "Generating inbound IntAuth value");
         Sk_P    := Tkmrpc.Contexts.ae.get_sk_ike_auth_rem (Id => Ae_Id);
         IntAuth := Tkmrpc.Contexts.ae.get_intauth_rem (Id => Ae_Id);
      else
         L.Log (Message => "Generating outbound IntAuth value");
         Sk_P    := Tkmrpc.Contexts.ae.get_sk_ike_auth_loc (Id => Ae_Id);
         IntAuth := Tkmrpc.Contexts.ae.get_intauth_loc (Id => Ae_Id);
      end if;

      Blob.Read
        (Id     => Data_Id,
         Offset => 0,
         Length => Data'Length,
         Data   => Data);
      Blob.Reset (Id => Data_Id);

      declare
         Count       : constant Tkmrpc.Types.Intauth_Count_Type
           := Tkmrpc.Contexts.ae.get_intauth_count (Id => Ae_Id);
         IntAuth_Len : constant Natural
           := (if IntAuth.Size > 1 then IntAuth.Size else 0);

         Seed  : Tkmrpc.Types.Byte_Sequence
           (1 .. IntAuth_Len + Data'Length);
         Value : Tkmrpc.Types.Byte_Sequence
           (1 .. Crypto.Hmac_Sha512.Hash_Output_Length);
         --  Use PRF-HMAC-SHA512 for now.
         Prf   : Crypto.Hmac_Sha512.Context_Type;
      begin
         Seed (Seed'First .. IntAuth_Len)
           := IntAuth.Data (IntAuth.Data'First ..
                              IntAuth.Data'First + IntAuth_Len - 1);
         Seed (Seed'First + IntAuth_Len .. Seed'Last) := Data;

         Crypto.Hmac_Sha512.Init (Ctx => Prf,
                                  Key => Sk_P.Data (1 .. Sk_P.Size));
         Value := Crypto.Hmac_Sha512.Generate (Ctx  => Prf,
                                               Data => Seed);

         IntAuth.Size := Value'Length;
         IntAuth.Data (IntAuth.Data'First ..
                         IntAuth.Data'First + Value'Length - 1)
             := Value;

         if Inbound = 1 then
            Tkmrpc.Contexts.ae.set_intauth_rem (Id      => Ae_Id,
                                                intauth => IntAuth);
         else
            Tkmrpc.Contexts.ae.set_intauth_loc (Id      => Ae_Id,
                                                intauth => IntAuth);
         end if;
         Tkmrpc.Contexts.ae.set_intauth_count (Id    => Ae_Id,
                                               count => Count + 1);
         L.Log (Message => "IntAuth " & Utils.To_Hex_String
                (Input => Value));
      end;
   end Int_Auth;

   -------------------------------------------------------------------------

   procedure Reset (Isa_Id : Tkmrpc.Types.Isa_Id_Type)
   is
   begin
      L.Log (Message => "Resetting ISA context" & Isa_Id'Img);
      Tkmrpc.Contexts.isa.reset (Id => Isa_Id);
      Crypto.Aead.Reset (Isa_Id => Isa_Id);
   end Reset;

   -------------------------------------------------------------------------

   procedure Sign
     (Isa_Id       :     Tkmrpc.Types.Isa_Id_Type;
      Lc_Id        :     Tkmrpc.Types.Lc_Id_Type;
      Init_Message :     Tkmrpc.Types.Init_Message_Type;
      Signature    : out Tkmrpc.Types.Signature_Type)
   is
      package RSA renames Crypto.Rsa_Pkcs1_Sha1;

      Privkey : constant X509.Keys.RSA_Private_Key_Type := Private_Key.Get;
      Signer  : RSA.Signer_Type;
      Ae_Id   : constant Tkmrpc.Types.Ae_Id_Type
        := Tkmrpc.Contexts.isa.get_ae_id (Id => Isa_Id);
      Octets  : constant Tkmrpc.Types.Byte_Sequence
        := Compute_Auth_Octets
          (Ae_Id        => Ae_Id,
           Init_Message => Init_Message,
           Idx          => Config.Get_Local_Identity
             (Id => Tkmrpc.Types.Li_Id_Type (Lc_Id)).Name,
           Verify       => False);
   begin
      L.Log (Message => "Generating local signature for ISA context"
             & Isa_Id'Img);

      RSA.Init (Ctx   => Signer,
                N     => X509.Keys.Get_Modulus (Key => Privkey),
                E     => X509.Keys.Get_Pub_Exponent (Key => Privkey),
                D     => X509.Keys.Get_Priv_Exponent (Key => Privkey),
                P     => X509.Keys.Get_Prime_P (Key => Privkey),
                Q     => X509.Keys.Get_Prime_Q (Key => Privkey),
                Exp1  => X509.Keys.Get_Exponent1 (Key => Privkey),
                Exp2  => X509.Keys.Get_Exponent2 (Key => Privkey),
                Coeff => X509.Keys.Get_Coefficient (Key => Privkey));

      declare
         Sig : constant Tkmrpc.Types.Byte_Sequence
           := RSA.Generate (Ctx  => Signer,
                            Data => Octets);
      begin
         Signature.Data (1 .. Sig'Length) := Sig;
         Signature.Size                   := Sig'Length;

         L.Log (Message => "Signature " & Utils.To_Hex_String
                (Input => Sig));
         Tkmrpc.Contexts.ae.sign
           (Id    => Ae_Id,
            lc_id => Lc_Id);
      end;
   end Sign;

   -------------------------------------------------------------------------

   procedure Skip_Create_First (Isa_Id : Tkmrpc.Types.Isa_Id_Type)
   is
      Ae_Id : constant Tkmrpc.Types.Ae_Id_Type
        := Tkmrpc.Contexts.isa.get_ae_id (Id => Isa_Id);
   begin
      Tkmrpc.Contexts.ae.activate (Id => Ae_Id);
   end Skip_Create_First;

   -------------------------------------------------------------------------

   procedure Update
     (Isa_Id : Tkmrpc.Types.Isa_Id_Type;
      Ke_Id  : Tkmrpc.Types.Ke_Id_Type)
   is
      use type Tkmrpc.Types.Init_Type;

      Ae_Id     : constant Tkmrpc.Types.Ae_Id_Type
        := Tkmrpc.Contexts.isa.get_ae_id (Id => Isa_Id);
      Old_Sk_D  : constant Tkmrpc.Types.Key_Type
        := Tkmrpc.Contexts.isa.get_sk_d (Id => Isa_Id);
      Initiator : constant Tkmrpc.Types.Init_Type
        := Tkmrpc.Contexts.ae.is_initiator (Id => Ae_Id);
      Nonce_Loc : constant Tkmrpc.Types.Nonce_Type
        := Tkmrpc.Contexts.ae.get_nonce_loc (Id => Ae_Id);
      Nonce_Rem : constant Tkmrpc.Types.Nonce_Type
        := Tkmrpc.Contexts.ae.get_nonce_rem (Id => Ae_Id);
      Spi_Loc   : constant Tkmrpc.Types.Ike_Spi_Type
        := Tkmrpc.Contexts.ae.get_spi_loc (Id => Ae_Id);
      Spi_Rem   : constant Tkmrpc.Types.Ike_Spi_Type
        := Tkmrpc.Contexts.ae.get_spi_rem (Id => Ae_Id);

      Sk                 : Tkmrpc.Types.Ke_Key_Type;
      Sk_Ai, Sk_Ar       : Tkmrpc.Types.Key_Type
        := (Size => Crypto.Aead.Int_Key_Length,
            Data => (others => 0));
      Sk_Ei, Sk_Er       : Tkmrpc.Types.Key_Type
        := (Size => Crypto.Aead.Enc_Key_Length,
            Data => (others => 0));
      Sk_D, Sk_Pi, Sk_Pr : Tkmrpc.Types.Key_Type
        := (Size => Crypto.Hmac_Sha512.Hash_Output_Length,
            Data => (others => 0));
   begin
      L.Log (Message => "Updating ISA context with ID" & Isa_Id'Img
             & " (KE" & Ke_Id'Img & ")");

      Tkm.Servers.Ike.Ke.Generate (Id => Ke_Id);

      Tkmrpc.Contexts.ke.consume (Id         => Ke_Id,
                                  shared_key => Sk);
      L.Log (Message => "KE context" & Ke_Id'Img & " consumed");

      Derive_Child_Keys
        (Old_Sk_D  => Old_Sk_D,
         Sk_0      => Sk.Data
           (Sk.Data'First .. Sk.Data'First + Sk.Size - 1),
         Sk_1n     => Null_Byte_Sequence,
         Initiator => Initiator,
         Nonce_Loc => Nonce_Loc,
         Nonce_Rem => Nonce_Rem,
         Spi_Loc   => Spi_Loc,
         Spi_Rem   => Spi_Rem,
         Sk_D      => Sk_D,
         Sk_Ai     => Sk_Ai,
         Sk_Ar     => Sk_Ar,
         Sk_Ei     => Sk_Ei,
         Sk_Er     => Sk_Er,
         Sk_Pi     => Sk_Pi,
         Sk_Pr     => Sk_Pr);

      --  Update ae, isa and AEAD contexts

      Tkmrpc.Contexts.ae.update
        (Id              => Ae_Id,
         sk_ike_auth_loc => (if Initiator = 1 then Sk_Pi else Sk_Pr),
         sk_ike_auth_rem => (if Initiator = 0 then Sk_Pi else Sk_Pr));
      Tkmrpc.Contexts.isa.set_sk_d
        (Id    => Isa_Id,
         sk_d  => Sk_D);
      Crypto.Aead.Create
        (Isa_Id   => Isa_Id,
         Sk_A_In  => (if Initiator = 0 then Sk_Ai else Sk_Ar),
         Sk_A_Out => (if Initiator = 1 then Sk_Ai else Sk_Ar),
         Sk_E_In  => (if Initiator = 0 then Sk_Ei else Sk_Er),
         Sk_E_Out => (if Initiator = 1 then Sk_Ei else Sk_Er));
   end Update;

end Tkm.Servers.Ike.Isa;
