--
--  Copyright (C) 2013  Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2013  Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tkmrpc.Types;

with Tkm.Key_Derivation;
with Tkm.Utils;

package body Key_Derivation_Tests
is

   use Ahven;
   use Tkm;

   -------------------------------------------------------------------------

   procedure Derive_Child_Keys
   is
      use type Tkmrpc.Types.Byte_Sequence;

      Sk_D    : constant Tkmrpc.Types.Byte_Sequence := Utils.Hex_To_Bytes
        (Input => "70e889b685ae906f3700c27f11b5633152c0cd582e989d18e54d405e84b"
         & "30b5a12c11af8c2a8228defe54e82537321a607412dee7eafc898ecce592333753"
         & "332");
      Nc_I    : constant Tkmrpc.Types.Byte_Sequence := Utils.Hex_To_Bytes
        (Input => "1ad3b9306b750c153b2e0aa08ccf132569affbb6656d03151aee8bbbd02"
         & "8f0b7");
      Nc_R    : constant Tkmrpc.Types.Byte_Sequence := Utils.Hex_To_Bytes
        (Input => "026cc399c78bf7cd0a2ec0cdc970a2913516f36a939daf2cbec840f2c09"
         & "f44d1");
      Ref_E_I : constant Tkmrpc.Types.Byte_Sequence := Utils.Hex_To_Bytes
        (Input => "7fa67ca92f9c863778092a0e381ecc2a72af0d302a5466521b9085d3bcb"
         & "02b7d");
      Ref_E_R : constant Tkmrpc.Types.Byte_Sequence := Utils.Hex_To_Bytes
        (Input => "88429bd9569148405be68f5ff94023d9edc2c1891d1361da04bd891add3"
         & "f2094");
      Ref_I_I : constant Tkmrpc.Types.Byte_Sequence := Utils.Hex_To_Bytes
        (Input => "1aba5f3c7fa0336977305518d3a6d9918cd7b35c2db3f019931db875c48"
         & "50f01ee5a6a17dbe3ae5a8b73f806b701b7db419e3c5ed308b62e8c085f86a6aeb"
         & "e7a");
      Ref_I_R : constant Tkmrpc.Types.Byte_Sequence := Utils.Hex_To_Bytes
        (Input => "95faec08b38d8d36342ff4f316ca8a1efc83ed1c4693f4492f20b1ac29a"
         & "f608c8aa092e1af507a31c3f8d7857f8eb490725c4d54224eb7eb0f7bccb3b5d9b"
         & "dc9");

      E_I, E_R, I_I, I_R : Tkmrpc.Types.Key_Type := Tkmrpc.Types.Null_Key_Type;
   begin
      Key_Derivation.Derive_Child_Keys
        (Sk_D    => Sk_D,
         Sk_0    => Null_Byte_Sequence,
         Sk_1n   => Null_Byte_Sequence,
         Nonce_I => Nc_I,
         Nonce_R => Nc_R,
         Enc_I   => E_I,
         Enc_R   => E_R,
         Int_I   => I_I,
         Int_R   => I_R);

      Assert (Condition => E_I.Data (1 .. E_I.Size) = Ref_E_I,
              Message   => "Enc_I mismatch");
      Assert (Condition => E_R.Data (1 .. E_R.Size) = Ref_E_R,
              Message   => "Enc_R mismatch");
      Assert (Condition => I_I.Data (1 .. I_I.Size) = Ref_I_I,
              Message   => "Int_I mismatch");
      Assert (Condition => I_R.Data (1 .. I_R.Size) = Ref_I_R,
              Message   => "Int_R mismatch");
   end Derive_Child_Keys;

   -------------------------------------------------------------------------

   procedure Derive_Child_Keys_Multi_Ke
   is
      use type Tkmrpc.Types.Byte_Sequence;

      Sk_D    : constant Tkmrpc.Types.Byte_Sequence := Utils.Hex_To_Bytes
        (Input => "4dc3fe98f18daa093414b235245cfaa72397b2a2a3459e227262c23786a"
         & "4430bfec5f1d2f085f4d654bc744bcc5b5f6fb0c68424a89958d38e9aebeb0129c"
         & "5d1");
      Sk_0    : constant Tkmrpc.Types.Byte_Sequence := Utils.Hex_To_Bytes
        (Input => "61a00806ada1143984705909693110b5a15eef14f66222f728f286fe8d4"
         & "943fbc2e92ba56dd30d514e0dcba26f22c7859dd0934c29f4d178d0e66182d03f1"
         & "2e708264b949bf8a1c1a68bbe70f27e9457b640c0136c64d1860373c211fdd833c"
         & "e13056586f371fb41c610baeb2f6846f051eca0ef2f2d68c636869c6a06c00fb1b"
         & "2500f8697033c5932bb5a6201429914defa47d585386f2cdfc069f37383bed0666"
         & "e23fe581227aa0a396d68df6f9f7fcab5e40082766e58e02aaaf2899bad815f041"
         & "24a62559a9afbd8488881126df0e6b32d01e3f69a68b823f43f7c9a70ce04a3451"
         & "3cf80ab192b284324b4e66e8f3a8f33060228f99ac140416af76cfd9efc6b27342"
         & "7ec0d75788610e2624f8d16bc4fef1ec9b91c1d246e83881b38a72315d12332838"
         & "0b336a9456ce7a1a84729068b65a22b355d2334995dfa6652db60e6c6516168ec3"
         & "99240ec345318d9e369b23562f485945301f07e53f53ba9ce8b10bfca1e6b406fc"
         & "192ff1ff48139792f81917767450981c6b690542958c17685");
      Sk_1n   : constant Tkmrpc.Types.Byte_Sequence := Utils.Hex_To_Bytes
        (Input => "416a4cc41251c78ccaae2990752a668372a500cc195c291b80c8c98168c"
         & "1a714e9fb261f27e254c3bc8d227db6706d58f82ec9cdacd6f850ec9330d367848"
         & "db49ba6b85cc11aec7dc5eb24a87f310eec724d159f153476e5d5c2e5870c13833"
         & "cef1dea7ca5375d2a3ae068115d62e0552d42c721d5ad8054d26482b0a96d0211f"
         & "3d9b04ca225bda0c51a25d1bb10a9043054eacc11807c9f66cfc855f677b5c5bd2"
         & "f2eae3033c98422d9f8dc7ff07850a2d470c479e2dab5b2685cfe2ce5f8ecd670b"
         & "3d38d857edd7bdd7ead0e0cdf1b5e8686a48986c154033f79e3d74d3c8473ec594"
         & "3d4c0516d76f1958247838394fd0bdf0256e718bcf04258e84d4e114cdf47d9b94"
         & "042e8e44daa8810753d5badbe193e82bbb8393059e020de5ceb51f800c1ac4be07"
         & "5a3f41e3fccc688e193c3f40b1ae7f1e8573af79ba7799fd9029b0e6027b52be7e"
         & "24d478225f9aff860e17812a3a1dce6cf73c2641bc711b81612c1093b983ee5900"
         & "335db7c26e53a530d52d26531ea5098413762a80532a742810d922dd3a4d61ff92"
         & "7d80b0c713556215410cf0483ddaa24253b0c52bfa0aaf6fdd0dcca426a8cb570f"
         & "df0395cd244983f89b63d7d3360b83b3941558a50ddc2c05e9b1d82df5cdfbfb5f"
         & "75880b711bf30131ba1cb26490139153c6fddd44f9f6f3dfdb455b9acd67f9a377"
         & "374545053a64105c48435b95a2fac8d5636ed0001");
      Nc_I    : constant Tkmrpc.Types.Byte_Sequence := Utils.Hex_To_Bytes
        (Input => "97d41c88fece82a3ce03dd550ed7a5b318aae4252c1ea85b792c56cfe1e"
         & "5fe73");
      Nc_R    : constant Tkmrpc.Types.Byte_Sequence := Utils.Hex_To_Bytes
        (Input => "3f67a06fde82489a96b8cd5a6e3b0a09b34d995f209ab9afe8316277fb4"
         & "cb239");
      Ref_E_I : constant Tkmrpc.Types.Byte_Sequence := Utils.Hex_To_Bytes
        (Input => "074aa1e4951b564265b35e780166019b1d59da4b127a07873bdf674a0db"
         & "b4234");
      Ref_E_R : constant Tkmrpc.Types.Byte_Sequence := Utils.Hex_To_Bytes
        (Input => "5522d4a21ca7799169383471923c04b20933626e428c6d34d57066520d1"
         & "313ac");
      Ref_I_I : constant Tkmrpc.Types.Byte_Sequence := Utils.Hex_To_Bytes
        (Input => "31f3c9e8c9f524b9847b0c29a763a65c478a287ad82df43a7477938605b"
         & "e4d4ae32036e5615261f1159dcbda44f41d29331f243045f9db211e1e448a02d1a"
         & "81a");
      Ref_I_R : constant Tkmrpc.Types.Byte_Sequence := Utils.Hex_To_Bytes
        (Input => "8e34e8ea2945b5a19f86ac699586d07583c5bcb4bdeb840b271f12f3056"
         & "085e0ffd196d5db53dcba86ca2abf936c175b72830e84625d03b349c9475af8e60"
         & "da6");

      E_I, E_R, I_I, I_R : Tkmrpc.Types.Key_Type := Tkmrpc.Types.Null_Key_Type;
   begin
      Key_Derivation.Derive_Child_Keys
        (Sk_D    => Sk_D,
         Sk_0    => Sk_0,
         Sk_1n   => Sk_1n,
         Nonce_I => Nc_I,
         Nonce_R => Nc_R,
         Enc_I   => E_I,
         Enc_R   => E_R,
         Int_I   => I_I,
         Int_R   => I_R);

      Assert (Condition => E_I.Data (1 .. E_I.Size) = Ref_E_I,
              Message   => "Enc_I mismatch");
      Assert (Condition => E_R.Data (1 .. E_R.Size) = Ref_E_R,
              Message   => "Enc_R mismatch");
      Assert (Condition => I_I.Data (1 .. I_I.Size) = Ref_I_I,
              Message   => "Int_I mismatch");
      Assert (Condition => I_R.Data (1 .. I_R.Size) = Ref_I_R,
              Message   => "Int_R mismatch");
   end Derive_Child_Keys_Multi_Ke;
   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Key derivation tests");
      T.Add_Test_Routine
        (Routine => Derive_Child_Keys'Access,
         Name    => "Child key derivation");
      T.Add_Test_Routine
        (Routine => Derive_Child_Keys_Multi_Ke'Access,
         Name    => "Child key derivation with multiple KEs");
   end Initialize;

end Key_Derivation_Tests;
