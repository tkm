--
--  Copyright (C) 2021  Tobias Brunner <tobias@codelabs.ch>
--  Copyright (C) 2013  Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2013  Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Assertions;

with Tkmrpc.Results;
with Tkmrpc.Types;
with Tkmrpc.Servers.Ike;
with Tkmrpc.Contexts.ke;

with Tkm.Servers.Ike.Blob;
with Tkm.Servers.Ike.Ke;

package body Server_Ike_KE_Tests is

   use Ahven;
   use Tkmrpc;

   -------------------------------------------------------------------------

   procedure Check_Operations_Initiator
   is
      use type Tkmrpc.Results.Result_Type;
      use type Tkmrpc.Types.Blob_Length_Type;
      use type Tkmrpc.Types.Blob_Out_Bytes_Type;
      use type Tkmrpc.Contexts.ke.ke_State_Type;
      use type Tkm.Servers.Ike.Blob.Blob_State_Type;

      Id      : constant := 11;
      Res     : Results.Result_Type;
      Pub_Id  : constant := 8;
      Pub_Id2 : constant := 9;
      Pub_Len : Types.Blob_Length_Type;
      Pub_Out : Types.Blob_Out_Bytes_Type;
      Pub_In  : constant Types.Blob_In_Bytes_Type
        := (Size => 512,
            Data => (others => 0));

      Null_Bytes : constant Types.Blob_Out_Bytes_Type := (others => 0);
   begin
      Servers.Ike.Init;
      Servers.Ike.Ke_Get
        (Result      => Res,
         Ke_Id       => Id,
         Kea_Id      => 2,
         Pubvalue_Id => Pub_Id,
         Ke_Length   => Pub_Len);
      Assert (Condition => Res = Results.Ok,
              Message   => "Ke_Get failed");
      Assert (Condition => Pub_Len = 512,
              Message   => "Public value size mismatch");
      Assert (Condition => Contexts.ke.Get_State
              (Id => Id) = Contexts.ke.initiator,
              Message   => "ke context not initiator");

      begin
         Servers.Ike.Ke_Get
           (Result      => Res,
            Ke_Id       => Id,
            Kea_Id      => 2,
            Pubvalue_Id => Pub_Id,
            Ke_Length   => Pub_Len);
         Fail (Message => "Exception expected if calling Ke_Get twice");
      exception
         when Ada.Assertions.Assertion_Error => null;
      end;

      Servers.Ike.Blob_Read
        (Result    => Res,
         Blob_Id   => Pub_Id,
         Offset    => 0,
         Length    => Pub_Len,
         Blob_Data => Pub_Out);
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Read failed");
      Assert (Condition => Pub_Out /= Null_Bytes,
              Message   => "Public data is nil");

      --  Use own pubvalue for other side too.

      Servers.Ike.Ke_Set
        (Result      => Res,
         Ke_Id       => Id,
         Kea_Id      => 2,
         Pubvalue_Id => Pub_Id);
      Assert (Condition => Res = Results.Ok,
              Message   => "Ke_Set failed");
      Assert (Condition => Contexts.ke.Get_State
              (Id => Id) = Contexts.ke.provisioned,
              Message   => "ke context not provisioned");
      Assert (Condition => Tkm.Servers.Ike.Blob.Get_State (Id => Pub_Id) =
              Tkm.Servers.Ike.Blob.Clean,
              Message   => "blob context not clean");

      Servers.Ike.Blob_Create
        (Result  => Res,
         Blob_Id => Pub_Id2,
         Length  => Types.Blob_Length_Type (Pub_In.Size));
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Create failed");

      Servers.Ike.Blob_Write
        (Result    => Res,
         Blob_Id   => Pub_Id2,
         Offset    => 0,
         Blob_Data => Pub_In);
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Write failed");

      begin
         Servers.Ike.Ke_Set
           (Result      => Res,
            Ke_Id       => Id,
            Kea_Id      => 2,
            Pubvalue_Id => Pub_Id2);
         Fail (Message => "Exception expected if calling Ke_Set twice");
      exception
         when Ada.Assertions.Assertion_Error =>
            Servers.Ike.Blob_Reset (Result  => Res,
                                    Blob_Id => Pub_Id2);
      end;

      --  This happens internally when creating ISA/ESA
      Tkm.Servers.Ike.Ke.Generate (Id => Id);

      begin
         Tkm.Servers.Ike.Ke.Generate (Id => Id);
         Fail (Message => "Exception expected if calling Generate twice");
      exception
         when Ada.Assertions.Assertion_Error => null;
      end;

      Servers.Ike.Ke_Reset (Result => Res,
                            Ke_Id  => Id);
      Assert (Condition => Res = Results.Ok,
              Message   => "Ke_Reset failed");

      Servers.Ike.Finalize;

   exception
      when others =>
         Servers.Ike.Ke_Reset (Result => Res,
                               Ke_Id  => Id);
         Servers.Ike.Finalize;
         raise;
   end Check_Operations_Initiator;

   -------------------------------------------------------------------------

   procedure Check_Operations_Responder
   is
      use type Tkmrpc.Results.Result_Type;
      use type Tkmrpc.Types.Blob_Length_Type;
      use type Tkmrpc.Types.Blob_Out_Bytes_Type;
      use type Tkmrpc.Contexts.ke.ke_State_Type;
      use type Tkm.Servers.Ike.Blob.Blob_State_Type;

      Id      : constant := 11;
      Res     : Results.Result_Type;
      Pub_Id  : constant := 8;
      Pub_Id2 : constant := 9;
      Pub_Len : Types.Blob_Length_Type;
      Pub     : Types.Blob_Out_Bytes_Type;
      Peer    : constant Types.Blob_In_Bytes_Type
        := (Size  => 512,
            Data  => (others => 1));

      Null_Bytes : constant Types.Blob_Out_Bytes_Type := (others => 0);
   begin
      Servers.Ike.Init;

      Servers.Ike.Blob_Create
        (Result  => Res,
         Blob_Id => Pub_Id,
         Length  => Types.Blob_Length_Type (Peer.Size));
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Create failed (1)");

      Servers.Ike.Blob_Write
        (Result    => Res,
         Blob_Id   => Pub_Id,
         Offset    => 0,
         Blob_Data => Peer);
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Write failed (1)");

      Servers.Ike.Ke_Set
        (Result      => Res,
         Ke_Id       => Id,
         Kea_Id      => 2,
         Pubvalue_Id => Pub_Id);
      Assert (Condition => Res = Results.Ok,
              Message   => "Ke_Set failed");
      Assert (Condition => Contexts.ke.Get_State
              (Id => Id) = Contexts.ke.responder,
              Message   => "ke context not responder");
      Assert (Condition => Tkm.Servers.Ike.Blob.Get_State (Id => Pub_Id) =
              Tkm.Servers.Ike.Blob.Clean,
              Message   => "blob context not clean");

      --  This is actually redundant as Ke_Set resets the blob already

      Servers.Ike.Blob_Reset (Result => Res,
                              Blob_Id => Pub_Id);
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Reset failed (1)");

      Servers.Ike.Blob_Create
        (Result  => Res,
         Blob_Id => Pub_Id2,
         Length  => Types.Blob_Length_Type (Peer.Size));
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Create failed (2)");
      Servers.Ike.Blob_Write
        (Result    => Res,
         Blob_Id   => Pub_Id2,
         Offset    => 0,
         Blob_Data => Peer);
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Write failed (2)");

      begin
         Servers.Ike.Ke_Set
           (Result      => Res,
            Ke_Id       => Id,
            Kea_Id      => 2,
            Pubvalue_Id => Pub_Id2);
         Fail (Message => "Exception expected if calling Ke_Set twice");
      exception
         when Ada.Assertions.Assertion_Error =>
            Servers.Ike.Blob_Reset (Result  => Res,
                                    Blob_Id => Pub_Id2);
      end;

      Servers.Ike.Ke_Get
        (Result      => Res,
         Ke_Id       => Id,
         Kea_Id      => 2,
         Pubvalue_Id => Pub_Id,
         Ke_Length   => Pub_Len);
      Assert (Condition => Res = Results.Ok,
              Message   => "Ke_Get failed");
      Assert (Condition => Pub_Len = 512,
              Message   => "Public value size mismatch");
      Assert (Condition => Contexts.ke.Get_State
              (Id => Id) = Contexts.ke.provisioned,
              Message   => "ke context not provisioned");

      begin
         Servers.Ike.Ke_Get
           (Result      => Res,
            Ke_Id       => Id,
            Kea_Id      => 2,
            Pubvalue_Id => Pub_Id,
            Ke_Length   => Pub_Len);
         Fail (Message => "Exception expected if calling Ke_Get twice");
      exception
         when Ada.Assertions.Assertion_Error => null;
      end;

      Servers.Ike.Blob_Read
        (Result    => Res,
         Blob_Id   => Pub_Id,
         Offset    => 0,
         Length    => Pub_Len,
         Blob_Data => Pub);
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Read failed");
      Assert (Condition => Pub /= Null_Bytes,
              Message   => "Public data is nil");

      --  This happens internally when creating ISA/ESA
      Tkm.Servers.Ike.Ke.Generate (Id => Id);

      begin
         Tkm.Servers.Ike.Ke.Generate (Id => Id);
         Fail (Message => "Exception expected if calling Generate twice");
      exception
         when Ada.Assertions.Assertion_Error => null;
      end;

      Servers.Ike.Blob_Reset (Result  => Res,
                              Blob_Id => Pub_Id);
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Reset failed (2)");

      Servers.Ike.Ke_Reset (Result => Res,
                            Ke_Id  => Id);
      Assert (Condition => Res = Results.Ok,
              Message   => "Ke_Reset failed");

      Servers.Ike.Finalize;

   exception
      when others =>
         Servers.Ike.Ke_Reset (Result => Res,
                               Ke_Id  => Id);
         Servers.Ike.Finalize;
         raise;
   end Check_Operations_Responder;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "IKE server KE tests");
      T.Add_Test_Routine
        (Routine => Check_Operations_Initiator'Access,
         Name    => "Check initiator");
      T.Add_Test_Routine
        (Routine => Check_Operations_Responder'Access,
         Name    => "Check responder");
   end Initialize;

end Server_Ike_KE_Tests;
