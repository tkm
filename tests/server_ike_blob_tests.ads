--
--  Copyright (C) 2021  Tobias Brunner <tobias@codelabs.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ahven.Framework;

package Server_Ike_Blob_Tests is

   type Testcase is new Ahven.Framework.Test_Case with null record;

   procedure Initialize (T : in out Testcase);
   --  Initialize testcase.

   procedure Check_Create;
   --  Check read operation.

   procedure Check_Read;
   --  Check read operation.

   procedure Check_Read_Offset;
   --  Check read operation with offset.

   procedure Check_Reset;
   --  Check reset operation.

   procedure Check_Write;
   --  Check write operation.

   procedure Check_Write_Offset;
   --  Check write operation with offset.

end Server_Ike_Blob_Tests;
