--
--  Copyright (C) 2020  Tobias Brunner <tobias@codelabs.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tkmrpc.Types;

with Tkm.Crypto.Aead;

package body Aead_Tests
is

   use Ahven;
   use Tkm;
   use Tkm.Crypto;
   use type Tkmrpc.Types.Byte_Sequence;

   Ref_Sk_Ai                : constant Tkmrpc.Types.Byte_Sequence
     (1 .. Aead.Int_Key_Length)
     := (16#EA#, 16#09#, 16#E5#, 16#EE#, 16#C0#, 16#83#, 16#5F#, 16#E4#,
         16#9E#, 16#EE#, 16#BE#, 16#A5#, 16#40#, 16#61#, 16#EB#, 16#E2#,
         16#8C#, 16#7C#, 16#FA#, 16#68#, 16#4C#, 16#C1#, 16#9B#, 16#E8#,
         16#54#, 16#C7#, 16#01#, 16#F7#, 16#55#, 16#38#, 16#CC#, 16#02#,
         16#42#, 16#34#, 16#05#, 16#0E#, 16#47#, 16#61#, 16#05#, 16#AE#,
         16#7E#, 16#27#, 16#4C#, 16#52#, 16#3A#, 16#5B#, 16#4E#, 16#18#,
         16#27#, 16#90#, 16#21#, 16#F3#, 16#7D#, 16#7F#, 16#1A#, 16#20#,
         16#BF#, 16#DC#, 16#C3#, 16#26#, 16#6C#, 16#C6#, 16#47#, 16#44#);

   Ref_Sk_Ar                : constant Tkmrpc.Types.Byte_Sequence
     (1 .. Aead.Int_Key_Length)
     := (16#89#, 16#1B#, 16#30#, 16#B9#, 16#BD#, 16#7D#, 16#CB#, 16#1F#,
         16#B5#, 16#F2#, 16#42#, 16#6C#, 16#34#, 16#6A#, 16#4D#, 16#1C#,
         16#7B#, 16#86#, 16#E8#, 16#71#, 16#4F#, 16#85#, 16#6A#, 16#58#,
         16#F9#, 16#8B#, 16#5B#, 16#11#, 16#C0#, 16#B9#, 16#D6#, 16#E9#,
         16#9E#, 16#04#, 16#88#, 16#38#, 16#90#, 16#A4#, 16#6B#, 16#DB#,
         16#D7#, 16#2B#, 16#41#, 16#E7#, 16#78#, 16#DC#, 16#BE#, 16#D3#,
         16#20#, 16#F2#, 16#31#, 16#5A#, 16#6D#, 16#9A#, 16#84#, 16#EB#,
         16#CA#, 16#FE#, 16#DD#, 16#E1#, 16#F4#, 16#DD#, 16#D7#, 16#33#);

   Ref_Sk_Ei                : constant Tkmrpc.Types.Byte_Sequence
     (1 .. Aead.Enc_Key_Length)
     := (16#EA#, 16#75#, 16#60#, 16#D5#, 16#E3#, 16#FE#, 16#87#, 16#01#,
         16#13#, 16#9F#, 16#8B#, 16#F0#, 16#FF#, 16#E1#, 16#A8#, 16#E2#,
         16#A3#, 16#7B#, 16#D1#, 16#C0#, 16#F4#, 16#27#, 16#9B#, 16#FE#,
         16#62#, 16#08#, 16#D7#, 16#BC#, 16#39#, 16#8E#, 16#18#, 16#F8#);

   Ref_Sk_Er                : constant Tkmrpc.Types.Byte_Sequence
     (1 .. Aead.Enc_Key_Length)
     := (16#13#, 16#E8#, 16#6F#, 16#E2#, 16#07#, 16#4D#, 16#79#, 16#A1#,
         16#5E#, 16#33#, 16#41#, 16#9C#, 16#97#, 16#C7#, 16#04#, 16#CB#,
         16#3F#, 16#89#, 16#07#, 16#DC#, 16#11#, 16#4F#, 16#05#, 16#6A#,
         16#D1#, 16#49#, 16#55#, 16#66#, 16#CD#, 16#95#, 16#DE#, 16#8E#);

   -------------------------------------------------------------------------

   procedure Ike_Dpd
   is
      Ref_Iv                   : constant Tkmrpc.Types.Byte_Sequence
        (1 .. Aead.Iv_Length)
        := (16#F7#, 16#41#, 16#9F#, 16#3F#, 16#FE#, 16#09#, 16#88#, 16#02#,
            16#FB#, 16#FF#, 16#97#, 16#0E#, 16#56#, 16#E5#, 16#9D#, 16#94#);

      Ref_Aad_Plain            : constant Tkmrpc.Types.Byte_Sequence
        := (16#92#, 16#72#, 16#ED#, 16#72#, 16#51#, 16#7A#, 16#16#, 16#D8#,
            16#78#, 16#30#, 16#20#, 16#EE#, 16#CC#, 16#BD#, 16#6F#, 16#39#,
            16#2E#, 16#20#, 16#25#, 16#08#, 16#00#, 16#00#, 16#00#, 16#02#,
            16#00#, 16#00#, 16#00#, 16#60#, 16#00#, 16#00#, 16#00#, 16#44#,
            16#95#, 16#FC#, 16#39#, 16#5A#, 16#DA#, 16#2E#, 16#99#, 16#E0#,
            16#B4#, 16#7F#, 16#CA#, 16#01#, 16#65#, 16#DA#, 16#00#, 16#0F#);
      --  AAD, no data, random padding

      Ref_Iv_Encrypted_Icv     : constant Tkmrpc.Types.Byte_Sequence
        := (16#F7#, 16#41#, 16#9F#, 16#3F#, 16#FE#, 16#09#, 16#88#, 16#02#,
            16#FB#, 16#FF#, 16#97#, 16#0E#, 16#56#, 16#E5#, 16#9D#, 16#94#,
            16#B3#, 16#DA#, 16#38#, 16#F1#, 16#18#, 16#35#, 16#7A#, 16#37#,
            16#D5#, 16#5A#, 16#FE#, 16#FD#, 16#2E#, 16#55#, 16#E3#, 16#45#,
            16#C3#, 16#88#, 16#08#, 16#6F#, 16#14#, 16#DC#, 16#85#, 16#9C#,
            16#B5#, 16#E5#, 16#9A#, 16#F3#, 16#DD#, 16#63#, 16#9A#, 16#23#,
            16#03#, 16#EF#, 16#98#, 16#65#, 16#7B#, 16#4E#, 16#62#, 16#C3#,
            16#A2#, 16#C5#, 16#5D#, 16#65#, 16#21#, 16#95#, 16#C7#, 16#C4#);

      Ref_Aad_Iv_Encrypted_Icv : constant Tkmrpc.Types.Byte_Sequence
        := (16#92#, 16#72#, 16#ED#, 16#72#, 16#51#, 16#7A#, 16#16#, 16#D8#,
            16#78#, 16#30#, 16#20#, 16#EE#, 16#CC#, 16#BD#, 16#6F#, 16#39#,
            16#2E#, 16#20#, 16#25#, 16#20#, 16#00#, 16#00#, 16#00#, 16#02#,
            16#00#, 16#00#, 16#00#, 16#60#, 16#00#, 16#00#, 16#00#, 16#44#,
            --  AAD
            16#17#, 16#26#, 16#27#, 16#94#, 16#B6#, 16#15#, 16#53#, 16#C1#,
            16#68#, 16#83#, 16#1A#, 16#B9#, 16#B7#, 16#A2#, 16#C1#, 16#A6#,
            --  IV
            16#10#, 16#4E#, 16#88#, 16#5B#, 16#94#, 16#B2#, 16#23#, 16#86#,
            16#C1#, 16#18#, 16#39#, 16#F0#, 16#50#, 16#C0#, 16#5E#, 16#C4#,
            --  Encrypted padding
            16#56#, 16#29#, 16#2B#, 16#A1#, 16#57#, 16#34#, 16#EA#, 16#29#,
            16#54#, 16#BF#, 16#84#, 16#7B#, 16#A1#, 16#08#, 16#14#, 16#B2#,
            16#B2#, 16#A5#, 16#5F#, 16#AA#, 16#45#, 16#CE#, 16#49#, 16#6E#,
            16#62#, 16#A9#, 16#EE#, 16#1C#, 16#DE#, 16#73#, 16#09#, 16#CC#
            --  ICV
           );

      Ref_Decrypted            : constant Tkmrpc.Types.Byte_Sequence
        := (16#F7#, 16#58#, 16#11#, 16#2D#, 16#B2#, 16#4E#, 16#12#, 16#63#,
            16#FC#, 16#A5#, 16#78#, 16#0C#, 16#E4#, 16#03#, 16#71#, 16#0F#);
      --  Decrypted random padding

      Sk_A_In, Sk_A_Out, Sk_E_In, Sk_E_Out : Tkmrpc.Types.Key_Type;

      Aad_Plain            : Tkmrpc.Types.Aad_Plain_Type;
      Encrypted            : Tkmrpc.Types.Iv_Encrypted_Icv_Type;
      Aad_Iv_Encrypted_Icv : Tkmrpc.Types.Aad_Iv_Encrypted_Icv_Type;
      Decrypted            : Tkmrpc.Types.Decrypted_Type;
   begin
      Sk_A_In.Size                       := Ref_Sk_Ar'Length;
      Sk_A_In.Data (1 .. Sk_A_In.Size)   := Ref_Sk_Ar;
      Sk_A_Out.Size                      := Ref_Sk_Ai'Length;
      Sk_A_Out.Data (1 .. Sk_A_Out.Size) := Ref_Sk_Ai;
      Sk_E_In.Size                       := Ref_Sk_Er'Length;
      Sk_E_In.Data (1 .. Sk_E_In.Size)   := Ref_Sk_Er;
      Sk_E_Out.Size                      := Ref_Sk_Ei'Length;
      Sk_E_Out.Data (1 .. Sk_E_Out.Size) := Ref_Sk_Ei;

      Aead.Create
        (Isa_Id   => 1,
         Sk_A_In  => Sk_A_In,
         Sk_A_Out => Sk_A_Out,
         Sk_E_In  => Sk_E_In,
         Sk_E_Out => Sk_E_Out);

      Aad_Plain.Size                       := Ref_Aad_Plain'Length;
      Aad_Plain.Data (1 .. Aad_Plain.Size) := Ref_Aad_Plain;

      Aead.Encrypt
        (Isa_Id           => 1,
         Iv               => Ref_Iv,
         Aad_Len          => 32,
         Aad_Plain        => Aad_Plain,
         Iv_Encrypted_Icv => Encrypted);

      Assert (Condition => Encrypted.Size = Ref_Iv_Encrypted_Icv'Length,
              Message   => "Encryption size mismatch");

      Assert (Condition =>
                Encrypted.Data (1 .. Encrypted.Size) = Ref_Iv_Encrypted_Icv,
              Message   => "Encryption data mismatch");

      Aad_Iv_Encrypted_Icv.Size := Ref_Aad_Iv_Encrypted_Icv'Length;
      Aad_Iv_Encrypted_Icv.Data (1 .. Aad_Iv_Encrypted_Icv.Size)
        := Ref_Aad_Iv_Encrypted_Icv;

      Aead.Decrypt
        (Isa_Id               => 1,
         Aad_Len              => 32,
         Aad_Iv_Encrypted_Icv => Aad_Iv_Encrypted_Icv,
         Decrypted            => Decrypted);

      Assert (Condition => Decrypted.Size = Ref_Decrypted'Length,
              Message   => "Decryption size mismatch");

      Assert (Condition =>
                Decrypted.Data (1 .. Decrypted.Size) = Ref_Decrypted,
              Message   => "Decryption data mismatch");

      Aead.Reset (Isa_Id => 1);

      begin
         Aead.Decrypt
           (Isa_Id               => 1,
            Aad_Len              => 32,
            Aad_Iv_Encrypted_Icv => Aad_Iv_Encrypted_Icv,
            Decrypted            => Decrypted);
         Fail (Message => "AEAD error expected after reset");

      exception
         when Aead.Aead_Error => null;
      end;
   end Ike_Dpd;

   -------------------------------------------------------------------------

   procedure Ike_Dpd_Invalid
   is
      Ref_Aad_Iv_Encrypted_Icv : constant Tkmrpc.Types.Byte_Sequence
        := (16#92#, 16#72#, 16#ED#, 16#72#, 16#51#, 16#7A#, 16#16#, 16#D8#,
            16#78#, 16#30#, 16#20#, 16#EE#, 16#CC#, 16#BD#, 16#6F#, 16#39#,
            16#2E#, 16#20#, 16#25#, 16#20#, 16#00#, 16#00#, 16#00#, 16#02#,
            16#00#, 16#00#, 16#00#, 16#60#, 16#00#, 16#00#, 16#00#, 16#44#,
            --  AAD
            16#17#, 16#26#, 16#27#, 16#94#, 16#B6#, 16#15#, 16#53#, 16#C1#,
            16#68#, 16#83#, 16#1A#, 16#B9#, 16#B7#, 16#A2#, 16#C1#, 16#A6#,
            --  IV
            16#10#, 16#4E#, 16#88#, 16#5B#, 16#94#, 16#B2#, 16#23#, 16#86#,
            16#C1#, 16#18#, 16#39#, 16#F0#, 16#50#, 16#C0#, 16#5E#, 16#C4#,
            --  Encrypted padding
            16#01#, 16#02#, 16#03#, 16#04#, 16#05#, 16#06#, 16#07#, 16#08#,
            16#54#, 16#BF#, 16#84#, 16#7B#, 16#A1#, 16#08#, 16#14#, 16#B2#,
            16#B2#, 16#A5#, 16#5F#, 16#AA#, 16#45#, 16#CE#, 16#49#, 16#6E#,
            16#62#, 16#A9#, 16#EE#, 16#1C#, 16#DE#, 16#73#, 16#09#, 16#CC#
            --  ICV (invalid)
           );

      Sk_A_In, Sk_A_Out, Sk_E_In, Sk_E_Out : Tkmrpc.Types.Key_Type;

      Aad_Iv_Encrypted_Icv : Tkmrpc.Types.Aad_Iv_Encrypted_Icv_Type;
      Decrypted            : Tkmrpc.Types.Decrypted_Type;
   begin
      Sk_A_In.Size                       := Ref_Sk_Ar'Length;
      Sk_A_In.Data (1 .. Sk_A_In.Size)   := Ref_Sk_Ar;
      Sk_A_Out.Size                      := Ref_Sk_Ai'Length;
      Sk_A_Out.Data (1 .. Sk_A_Out.Size) := Ref_Sk_Ai;
      Sk_E_In.Size                       := Ref_Sk_Er'Length;
      Sk_E_In.Data (1 .. Sk_E_In.Size)   := Ref_Sk_Er;
      Sk_E_Out.Size                      := Ref_Sk_Ei'Length;
      Sk_E_Out.Data (1 .. Sk_E_Out.Size) := Ref_Sk_Ei;

      Aead.Create
        (Isa_Id   => 1,
         Sk_A_In  => Sk_A_In,
         Sk_A_Out => Sk_A_Out,
         Sk_E_In  => Sk_E_In,
         Sk_E_Out => Sk_E_Out);

      Aad_Iv_Encrypted_Icv.Size := Ref_Aad_Iv_Encrypted_Icv'Length;
      Aad_Iv_Encrypted_Icv.Data (1 .. Aad_Iv_Encrypted_Icv.Size)
        := Ref_Aad_Iv_Encrypted_Icv;

      begin
         Aead.Decrypt
           (Isa_Id               => 1,
            Aad_Len              => 32,
            Aad_Iv_Encrypted_Icv => Aad_Iv_Encrypted_Icv,
            Decrypted            => Decrypted);
         Fail (Message => "AEAD error expected for invalid ICV");

      exception
         when Aead.Aead_Error => null;
      end;

      Aead.Reset (Isa_Id => 1);
   end Ike_Dpd_Invalid;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "AEAD tests (AES-256/HMAC-SHA-512-256)");
      T.Add_Test_Routine
        (Routine => No_Keys'Access,
         Name    => "No Keys");
      T.Add_Test_Routine
        (Routine => Ike_Dpd'Access,
         Name    => "IKEv2 DPD");
      T.Add_Test_Routine
        (Routine => Ike_Dpd_Invalid'Access,
         Name    => "IKEv2 DPD invalid ICV");
   end Initialize;

   -------------------------------------------------------------------------

   procedure No_Keys
   is
      Dummy_Iv        : constant Tkmrpc.Types.Byte_Sequence
        (1 .. Aead.Iv_Length)
        := (others => 0);
      Dummy_Aad_Plain : constant Tkmrpc.Types.Aad_Plain_Type
        := (Size => 16,
            Data => (others => 0));
      Dummy_AIEI      : constant Tkmrpc.Types.Aad_Iv_Encrypted_Icv_Type
        := (Size => 8 + Aead.Iv_Length + 8 + Aead.Icv_Length,
            Data => (others => 0));
      Dummy_IEI       : Tkmrpc.Types.Iv_Encrypted_Icv_Type;
      Dummy_Decrypted : Tkmrpc.Types.Decrypted_Type;
   begin
      begin
         Aead.Encrypt
           (Isa_Id           => 1,
            Iv               => Dummy_Iv,
            Aad_Len          => 8,
            Aad_Plain        => Dummy_Aad_Plain,
            Iv_Encrypted_Icv => Dummy_IEI);
         Fail (Message => "AEAD error expected");

      exception
         when Aead.Aead_Error => null;
      end;

      begin
         Aead.Decrypt
           (Isa_Id               => 1,
            Aad_Len              => 8,
            Aad_Iv_Encrypted_Icv => Dummy_AIEI,
            Decrypted            => Dummy_Decrypted);
         Fail (Message => "AEAD error expected");

      exception
         when Aead.Aead_Error => null;
      end;
   end No_Keys;

end Aead_Tests;
