--
--  Copyright (C) 2020  Tobias Brunner <tobias@codelabs.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

package body Tkm.Crypto.Aead.Data
is

   procedure Get_Keys
     (Isa_Id   : Tkmrpc.Types.Isa_Id_Type;
      Sk_A_In  : out Tkmrpc.Types.Key_Type;
      Sk_A_Out : out Tkmrpc.Types.Key_Type;
      Sk_E_In  : out Tkmrpc.Types.Key_Type;
      Sk_E_Out : out Tkmrpc.Types.Key_Type)
   is
   begin
      Sk_A_In  := Context_Array (Isa_Id).Sk_A_In;
      Sk_A_Out := Context_Array (Isa_Id).Sk_A_Out;
      Sk_E_In  := Context_Array (Isa_Id).Sk_E_In;
      Sk_E_Out := Context_Array (Isa_Id).Sk_E_Out;
   end Get_Keys;

end Tkm.Crypto.Aead.Data;
