--
--  Copyright (C) 2021  Tobias Brunner <tobias@codelabs.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Exceptions;

with Tkmrpc.Types;
with Tkmrpc.Results;
with Tkmrpc.Servers.Ike;

with Tkm.Servers.Ike.Blob;

package body Server_Ike_Blob_Tests is

   use Ahven;
   use Tkmrpc;

   -------------------------------------------------------------------------

   procedure Check_Create
   is
      use type Results.Result_Type;
      use type Types.Blob_Length_Type;

      Id  : constant := 11;
      Res : Results.Result_Type;
   begin
      Servers.Ike.Blob_Create
        (Result  => Res,
         Blob_Id => Id,
         Length  => 10);
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Create failed (1)");

      begin
         Servers.Ike.Blob_Create
           (Result  => Res,
            Blob_Id => Id,
            Length  => 10);
         Fail (Message => "Exception expected if already in use");
      exception
         when E : Tkm.Servers.Ike.Blob.Blob_Failure =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "Blob context with ID 11 already in use",
                    Message   => "Exception message mismatch if already used");
      end;

      Servers.Ike.Blob_Reset
        (Result  => Res,
         Blob_Id => Id);
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Reset failed (1)");

      begin
         Servers.Ike.Blob_Create
           (Result  => Res,
            Blob_Id => Id,
            Length  => 1024 * 1024);
         Fail (Message => "Exception expected if too large");
      exception
         when E : Tkm.Servers.Ike.Blob.Blob_Failure =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "Unable to create blob context with ID 11 and length" &
                      Types.Blob_Length_Type (1024 * 1024)'Img,
                    Message   => "Exception message mismatch if too large");
      end;

      --  This is the current internal buffer size
      Servers.Ike.Blob_Create
           (Result  => Res,
            Blob_Id => Id,
            Length  => 128 * 1024);
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Create failed (2)");

      begin
         Servers.Ike.Blob_Create
           (Result  => Res,
            Blob_Id => 42,
            Length  => 1);
         Fail (Message => "Exception expected if internal buffer is full");
      exception
         when E : Tkm.Servers.Ike.Blob.Blob_Failure =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "Unable to create blob context with ID 42 and length 1",
                    Message   => "Exception message mismatch if buffer full");
      end;

      Servers.Ike.Blob_Reset
        (Result  => Res,
         Blob_Id => Id);
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Reset failed (2)");

      Servers.Ike.Finalize;
   exception
      when others =>
         Tkm.Servers.Ike.Blob.Reset (Id => Id);
         Servers.Ike.Finalize;
         raise;
   end Check_Create;

   -------------------------------------------------------------------------

   procedure Check_Read
   is
      use type Results.Result_Type;

      Id   : constant := 11;
      Data : constant Types.Blob_In_Bytes_Type
        := (Size  => 128,
            Data  => (others => 1));
      Read : Types.Blob_Out_Bytes_Type;
      Res  : Results.Result_Type;
   begin
      begin
         Servers.Ike.Blob_Read
           (Result    => Res,
            Blob_Id   => Id,
            Offset    => 0,
            Length    => 1,
            Blob_Data => Read);
         Fail (Message => "Exception expected if reading uninitialized blob");
      exception
         when E : Tkm.Servers.Ike.Blob.Blob_Failure =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "Blob context with ID 11 is not initialized",
                    Message   => "Exception message mismatch if reading " &
                      "uninitialized");
      end;

      Servers.Ike.Blob_Create
        (Result  => Res,
         Blob_Id => Id,
         Length  => Types.Blob_Length_Type (Data.Size));
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Create failed (1)");

      Servers.Ike.Blob_Write
        (Result    => Res,
         Blob_Id   => Id,
         Offset    => 0,
         Blob_Data => Data);
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Write failed");

      begin
         Servers.Ike.Blob_Read
           (Result    => Res,
            Blob_Id   => Id,
            Offset    => Types.Blob_Offset_Type (Data.Size + 1),
            Length    => 1,
            Blob_Data => Read);
         Fail (Message => "Exception expected if offset is too high");
      exception
         when E : Tkm.Servers.Ike.Blob.Blob_Failure =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "Invalid offset when reading from blob context with " &
                      "ID 11",
                    Message   => "Exception message mismatch for high offset");
      end;

      begin
         Servers.Ike.Blob_Read
           (Result    => Res,
            Blob_Id   => Id,
            Offset    => Types.Blob_Offset_Type (Data.Size),
            Length    => 1,
            Blob_Data => Read);
         Fail (Message => "Exception expected if reading beyond blob");
      exception
         when E : Tkm.Servers.Ike.Blob.Blob_Failure =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "Invalid length when reading from blob context with " &
                      "ID 11",
                    Message   => "Exception message mismatch after reading " &
                      "beyond blob");
      end;

      --  This is fine due to lenght 0
      Servers.Ike.Blob_Read
           (Result    => Res,
            Blob_Id   => Id,
            Offset    => Types.Blob_Offset_Type (Data.Size),
            Length    => 0,
            Blob_Data => Read);
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Read failed (1)");

      begin
         Servers.Ike.Blob_Read
           (Result    => Res,
            Blob_Id   => Id,
            Offset    => 0,
            Length    => Types.Blob_Length_Type (Data.Size + 1),
            Blob_Data => Read);
         Fail (Message => "Exception expected if length is too high");
      exception
         when E : Tkm.Servers.Ike.Blob.Blob_Failure =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "Invalid length when reading from blob context with " &
                      "ID 11",
                    Message   => "Exception message mismatch if length too " &
                      "high");
      end;

      Servers.Ike.Blob_Reset
        (Result  => Res,
         Blob_Id => Id);
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Reset failed (1)");

      Servers.Ike.Blob_Create
        (Result  => Res,
         Blob_Id => Id,
         Length  => Types.Blob_Length_Type (Read'Length + 10));
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Create failed (2)");

      begin
         Servers.Ike.Blob_Read
           (Result    => Res,
            Blob_Id   => Id,
            Offset    => 0,
            Length    => Types.Blob_Length_Type (Read'Length + 1),
            Blob_Data => Read);
         Fail (Message => "Exception expected if length exceeds output data");
      exception
         when E : Tkm.Servers.Ike.Blob.Blob_Failure =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "Invalid length when reading from blob context with " &
                      "ID 11",
                    Message   => "Exception message mismatch if length " &
                      "exceeds output data");
      end;

      Servers.Ike.Blob_Read
        (Result    => Res,
         Blob_Id   => Id,
         Offset    => 0,
         Length    => Types.Blob_Length_Type (Read'Length),
         Blob_Data => Read);
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Read failed (2)");

      Servers.Ike.Blob_Reset
        (Result  => Res,
         Blob_Id => Id);
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Reset failed (2)");

      Servers.Ike.Finalize;
   exception
      when others =>
         Tkm.Servers.Ike.Blob.Reset (Id => Id);
         Servers.Ike.Finalize;
         raise;
   end Check_Read;

   -------------------------------------------------------------------------

   procedure Check_Read_Offset
   is
      use type Results.Result_Type;
      use type Types.Blob_Out_Bytes_Type;

      Id     : constant := 11;
      Data   : constant Types.Blob_In_Bytes_Type
        := (Size  => 32,
            Data  =>
              (16#01#, 16#02#, 16#03#, 16#04#, 16#05#, 16#06#, 16#07#, 16#08#,
               16#09#, 16#0A#, 16#0B#, 16#0C#, 16#0D#, 16#0E#, 16#0F#, 16#10#,
               16#11#, 16#12#, 16#13#, 16#14#, 16#15#, 16#16#, 16#17#, 16#18#,
               16#19#, 16#1A#, 16#1B#, 16#1C#, 16#1D#, 16#1E#, 16#1F#, 16#20#,
               others => 0));
      Read_1 : constant Types.Blob_Out_Bytes_Type
        := (16#01#, 16#02#, 16#03#, 16#04#, 16#05#, 16#06#, 16#07#, 16#08#,
            others => 0);
      Read_2 : constant Types.Blob_Out_Bytes_Type
        := (16#09#, 16#0A#, 16#0B#, 16#0C#, 16#0D#, 16#0E#, 16#0F#, 16#10#,
            16#11#, 16#12#, 16#13#, 16#14#, 16#15#, others => 0);
      Read_3 : constant Types.Blob_Out_Bytes_Type
        := (16#16#, 16#17#, 16#18#, 16#19#, 16#1A#, 16#1B#, 16#1C#, 16#1D#,
            16#1E#, 16#1F#, others => 0);
      Read_4 : constant Types.Blob_Out_Bytes_Type
        := (16#20#, others => 0);

      Read     : Types.Blob_Out_Bytes_Type;
      Res      : Results.Result_Type;
   begin
      Servers.Ike.Blob_Create
        (Result  => Res,
         Blob_Id => Id,
         Length  => Types.Blob_Length_Type (Data.Size));
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Create failed");

      Servers.Ike.Blob_Write
        (Result    => Res,
         Blob_Id   => Id,
         Offset    => 0,
         Blob_Data => Data);
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Write failed");

      Servers.Ike.Blob_Read
        (Result    => Res,
         Blob_Id   => Id,
         Offset    => 0,
         Length    => 8,
         Blob_Data => Read);
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Read failed (1)");
      Assert (Condition => Read = Read_1,
              Message   => "Blob data does not match (1)");

      Servers.Ike.Blob_Read
        (Result    => Res,
         Blob_Id   => Id,
         Offset    => 8,
         Length    => 13,
         Blob_Data => Read);
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Read failed (2)");
      Assert (Condition => Read = Read_2,
              Message   => "Blob data does not match (2)");

      Servers.Ike.Blob_Read
        (Result    => Res,
         Blob_Id   => Id,
         Offset    => 21,
         Length    => 10,
         Blob_Data => Read);
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Read failed (3)");
      Assert (Condition => Read = Read_3,
              Message   => "Blob data does not match (3)");

      Servers.Ike.Blob_Read
        (Result    => Res,
         Blob_Id   => Id,
         Offset    => 31,
         Length    => 1,
         Blob_Data => Read);
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Read failed (4)");
      Assert (Condition => Read = Read_4,
              Message   => "Blob data does not match (4)");

      Servers.Ike.Blob_Reset
        (Result  => Res,
         Blob_Id => Id);
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Reset failed");

      Servers.Ike.Finalize;
   exception
      when others =>
         Tkm.Servers.Ike.Blob.Reset (Id => Id);
         Servers.Ike.Finalize;
         raise;
   end Check_Read_Offset;

   -------------------------------------------------------------------------

   procedure Check_Reset
   is
      use type Results.Result_Type;
      use type Types.Byte_Sequence;

      Id_1   : constant := 11;
      Id_2   : constant := 2;
      Id_3   : constant := 9;
      Data_1 : constant Types.Blob_In_Bytes_Type
        := (Size  => 10,
            Data  => (others => 1));
      Data_2 : constant Types.Blob_In_Bytes_Type
        := (Size  => 23,
            Data  => (others => 2));
      Data_3 : constant Types.Blob_In_Bytes_Type
        := (Size  => 512,
            Data  => (others => 3));
      Read   : Types.Blob_Out_Bytes_Type;
      Res    : Results.Result_Type;
   begin
      Servers.Ike.Init;

      Servers.Ike.Blob_Create
        (Result  => Res,
         Blob_Id => Id_1,
         Length  => Types.Blob_Length_Type (Data_1.Size));
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Create failed (1)");
      Servers.Ike.Blob_Write
        (Result    => Res,
         Blob_Id   => Id_1,
         Offset    => 0,
         Blob_Data => Data_1);
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Write failed (1)");

      Servers.Ike.Blob_Create
        (Result  => Res,
         Blob_Id => Id_2,
         Length  => Types.Blob_Length_Type (Data_2.Size));
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Create failed (2)");
      Servers.Ike.Blob_Write
        (Result    => Res,
         Blob_Id   => Id_2,
         Offset    => 0,
         Blob_Data => Data_2);
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Write failed (2)");

      Servers.Ike.Blob_Create
        (Result  => Res,
         Blob_Id => Id_3,
         Length  => Types.Blob_Length_Type (Data_3.Size));
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Create failed (3)");
      Servers.Ike.Blob_Write
        (Result    => Res,
         Blob_Id   => Id_3,
         Offset    => 0,
         Blob_Data => Data_3);
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Write failed (3)");

      Servers.Ike.Blob_Read
        (Result    => Res,
         Blob_Id   => Id_2,
         Offset    => 0,
         Length    => Types.Blob_Length_Type (Data_2.Size),
         Blob_Data => Read);
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Read failed (1)");
      Assert (Condition => Types.Byte_Sequence (Read (1 .. Data_2.Size)) =
                Types.Byte_Sequence (Data_2.Data (1 .. Data_2.Size)),
              Message   => "Blob data does not match (1)");

      Servers.Ike.Blob_Reset
        (Result  => Res,
         Blob_Id => Id_2);
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Reset failed (1)");

      begin
         Servers.Ike.Blob_Read
           (Result    => Res,
            Blob_Id   => Id_2,
            Offset    => 0,
            Length    => Types.Blob_Length_Type (Data_2.Size),
            Blob_Data => Read);
         Fail (Message => "Exception expected if reading uninitialized blob");
      exception
         when E : Tkm.Servers.Ike.Blob.Blob_Failure =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "Blob context with ID 2 is not initialized",
                    Message   => "Exception message mismatch for " &
                      "uninitialized blob");
      end;

      Servers.Ike.Blob_Read
        (Result    => Res,
         Blob_Id   => Id_1,
         Offset    => 0,
         Length    => Types.Blob_Length_Type (Data_1.Size),
         Blob_Data => Read);
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Read failed (2)");
      Assert (Condition => Types.Byte_Sequence (Read (1 .. Data_1.Size)) =
                Types.Byte_Sequence (Data_1.Data (1 .. Data_1.Size)),
              Message   => "Blob data does not match (2)");

      Servers.Ike.Blob_Read
        (Result    => Res,
         Blob_Id   => Id_3,
         Offset    => 0,
         Length    => Types.Blob_Length_Type (Data_3.Size),
         Blob_Data => Read);
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Read failed (3)");
      Assert (Condition => Types.Byte_Sequence (Read (1 .. Data_3.Size)) =
                Types.Byte_Sequence (Data_3.Data (1 .. Data_3.Size)),
              Message   => "Blob data does not match (3)");

      Servers.Ike.Blob_Reset
        (Result  => Res,
         Blob_Id => Id_1);
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Reset failed (2)");

      Servers.Ike.Blob_Read
        (Result    => Res,
         Blob_Id   => Id_3,
         Offset    => 0,
         Length    => Types.Blob_Length_Type (Data_3.Size),
         Blob_Data => Read);
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Read failed (4)");
      Assert (Condition => Types.Byte_Sequence (Read (1 .. Data_3.Size)) =
                Types.Byte_Sequence (Data_3.Data (1 .. Data_3.Size)),
              Message   => "Blob data does not match (4)");

      Servers.Ike.Blob_Reset
        (Result  => Res,
         Blob_Id => Id_3);
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Reset failed (3)");

      Servers.Ike.Finalize;
   exception
      when others =>
         Tkm.Servers.Ike.Blob.Reset (Id => Id_1);
         Tkm.Servers.Ike.Blob.Reset (Id => Id_2);
         Tkm.Servers.Ike.Blob.Reset (Id => Id_3);
         Servers.Ike.Finalize;
         raise;
   end Check_Reset;

   -------------------------------------------------------------------------

   procedure Check_Write
   is
      use type Results.Result_Type;

      Id       : constant := 11;
      Data     : constant Types.Blob_In_Bytes_Type
        := (Size  => 128,
            Data  => (others => 1));
      Too_Long : constant Types.Blob_In_Bytes_Type
        := (Size  => 129,
            Data  => (others => 1));
      Res      : Results.Result_Type;
   begin
      begin
         Servers.Ike.Blob_Write
           (Result    => Res,
            Blob_Id   => Id,
            Offset    => 0,
            Blob_Data => Data);
         Fail (Message => "Exception expected if writing uninitialized blob");
      exception
         when E : Tkm.Servers.Ike.Blob.Blob_Failure =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "Blob context with ID 11 is not initialized",
                    Message   => "Exception message mismatch if writing " &
                      "uninitialized blob");
      end;

      Servers.Ike.Blob_Create
        (Result  => Res,
         Blob_Id => Id,
         Length  => Types.Blob_Length_Type (Data.Size));
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Create failed");

      begin
         Servers.Ike.Blob_Write
           (Result    => Res,
            Blob_Id   => Id,
            Offset    => Types.Blob_Offset_Type (Data.Size + 1),
            Blob_Data => Data);
         Fail (Message => "Exception expected if offset is too high");
      exception
         when E : Tkm.Servers.Ike.Blob.Blob_Failure =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "Invalid offset when writing to blob context with ID 11",
                    Message   => "Exception message mismatch if offset is " &
                      "too high");
      end;

      begin
         Servers.Ike.Blob_Write
           (Result    => Res,
            Blob_Id   => Id,
            Offset    => 0,
            Blob_Data => Too_Long);
         Fail (Message => "Exception expected if data is too long");
      exception
         when E : Tkm.Servers.Ike.Blob.Blob_Failure =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "Invalid length when writing to blob context with ID 11",
                    Message   => "Exception message mismatch if data is " &
                      "too long");
      end;

      begin
         Servers.Ike.Blob_Write
           (Result    => Res,
            Blob_Id   => Id,
            Offset    => 1,
            Blob_Data => Data);
         Fail (Message => "Exception expected if data is too long at offset");
      exception
         when E : Tkm.Servers.Ike.Blob.Blob_Failure =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "Invalid length when writing to blob context with ID 11",
                    Message   => "Exception message mismatch if data is too " &
                      "long at offset");
      end;

      Servers.Ike.Blob_Write
        (Result    => Res,
         Blob_Id   => Id,
         Offset    => 0,
         Blob_Data => Data);
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Write failed (1)");

      --  Writing twice is OK

      Servers.Ike.Blob_Write
        (Result    => Res,
         Blob_Id   => Id,
         Offset    => 0,
         Blob_Data => Data);
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Write failed (2)");

      Servers.Ike.Blob_Reset
        (Result  => Res,
         Blob_Id => Id);
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Reset failed");

      Servers.Ike.Finalize;
   exception
      when others =>
         Tkm.Servers.Ike.Blob.Reset (Id => Id);
         Servers.Ike.Finalize;
         raise;
   end Check_Write;

   -------------------------------------------------------------------------

   procedure Check_Write_Offset
   is
      use type Results.Result_Type;
      use type Types.Blob_Out_Bytes_Type;

      Id       : constant := 11;
      Length   : constant Types.Blob_Length_Type := 32;
      Data_1   : constant Types.Blob_In_Bytes_Type
        := (Size  => 8,
            Data  =>
              (16#01#, 16#02#, 16#03#, 16#04#, 16#05#, 16#06#, 16#07#, 16#08#,
               others => 1));
      Data_2   : constant Types.Blob_In_Bytes_Type
        := (Size  => 13,
            Data  =>
              (16#09#, 16#0A#, 16#0B#, 16#0C#, 16#0D#, 16#0E#, 16#0F#, 16#10#,
               16#11#, 16#12#, 16#13#, 16#14#, 16#15#, others => 2));
      Data_3   : constant Types.Blob_In_Bytes_Type
        := (Size  => 10,
            Data  =>
              (16#16#, 16#17#, 16#18#, 16#19#, 16#1A#, 16#1B#, 16#1C#, 16#1D#,
               16#1E#, 16#1F#, others => 3));
      Data_4   : constant Types.Blob_In_Bytes_Type
        := (Size  => 1,
            Data  => (others => 16#20#));
      Data_5   : constant Types.Blob_In_Bytes_Type
        := (Size  => 16,
            Data  =>
              (16#0B#, 16#0C#, 16#0D#, 16#0E#, 16#0F#, 16#10#, 16#11#, 16#12#,
               16#13#, 16#14#, 16#15#, 16#16#, 16#17#, 16#18#, 16#19#, 16#1A#,
               others => 5));
      Complete : constant Types.Blob_Out_Bytes_Type
        := (16#01#, 16#02#, 16#03#, 16#04#, 16#05#, 16#06#, 16#07#, 16#08#,
            16#09#, 16#0A#, 16#0B#, 16#0C#, 16#0D#, 16#0E#, 16#0F#, 16#10#,
            16#11#, 16#12#, 16#13#, 16#14#, 16#15#, 16#16#, 16#17#, 16#18#,
            16#19#, 16#1A#, 16#1B#, 16#1C#, 16#1D#, 16#1E#, 16#1F#, 16#20#,
            others => 0);
      Updated  : constant Types.Blob_Out_Bytes_Type
        := (16#01#, 16#02#, 16#03#, 16#04#, 16#05#, 16#06#, 16#07#, 16#08#,
            16#09#, 16#0A#, 16#01#, 16#02#, 16#03#, 16#04#, 16#05#, 16#06#,
            16#07#, 16#08#, 16#13#, 16#14#, 16#15#, 16#16#, 16#17#, 16#18#,
            16#19#, 16#1A#, 16#1B#, 16#1C#, 16#1D#, 16#1E#, 16#1F#, 16#20#,
            others => 0);
      Read     : Types.Blob_Out_Bytes_Type;
      Res      : Results.Result_Type;
   begin
      Servers.Ike.Blob_Create
        (Result  => Res,
         Blob_Id => Id,
         Length  => Length);
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Create failed");

      Servers.Ike.Blob_Write
        (Result    => Res,
         Blob_Id   => Id,
         Offset    => 0,
         Blob_Data => Data_1);
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Write failed (1)");

      Servers.Ike.Blob_Write
        (Result    => Res,
         Blob_Id   => Id,
         Offset    => Types.Blob_Offset_Type (Data_1.Size),
         Blob_Data => Data_2);
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Write failed (2)");

      Servers.Ike.Blob_Write
        (Result    => Res,
         Blob_Id   => Id,
         Offset    => Types.Blob_Offset_Type (Data_1.Size + Data_2.Size),
         Blob_Data => Data_3);
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Write failed (3)");

      Servers.Ike.Blob_Write
        (Result    => Res,
         Blob_Id   => Id,
         Offset    => Types.Blob_Offset_Type
           (Data_1.Size + Data_2.Size + Data_3.Size),
         Blob_Data => Data_4);
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Write failed (4)");

      Servers.Ike.Blob_Read
        (Result    => Res,
         Blob_Id   => Id,
         Offset    => 0,
         Length    => Length,
         Blob_Data => Read);
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Read failed (1)");
      Assert (Condition => Read = Complete,
              Message   => "Blob data does not match (1)");

      Servers.Ike.Blob_Write
        (Result    => Res,
         Blob_Id   => Id,
         Offset    => 10,
         Blob_Data => Data_5);
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Write failed (5)");

      Servers.Ike.Blob_Read
        (Result    => Res,
         Blob_Id   => Id,
         Offset    => 0,
         Length    => Length,
         Blob_Data => Read);
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Read failed (2)");
      Assert (Condition => Read = Complete,
              Message   => "Blob data does not match (2)");

      Servers.Ike.Blob_Write
        (Result    => Res,
         Blob_Id   => Id,
         Offset    => 10,
         Blob_Data => Data_1);
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Write failed (6)");

      Servers.Ike.Blob_Read
        (Result    => Res,
         Blob_Id   => Id,
         Offset    => 0,
         Length    => Length,
         Blob_Data => Read);
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Read failed (3)");
      Assert (Condition => Read = Updated,
              Message   => "Blob data does not match (3)");

      Servers.Ike.Blob_Reset
        (Result  => Res,
         Blob_Id => Id);
      Assert (Condition => Res = Results.Ok,
              Message   => "Blob_Reset failed");

      Servers.Ike.Finalize;
   exception
      when others =>
         Tkm.Servers.Ike.Blob.Reset (Id => Id);
         Servers.Ike.Finalize;
         raise;
   end Check_Write_Offset;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "IKE server blob tests");
      T.Add_Test_Routine
        (Routine => Check_Create'Access,
         Name    => "Check Blob_Create");
      T.Add_Test_Routine
        (Routine => Check_Write'Access,
         Name    => "Check Blob_Write");
      T.Add_Test_Routine
        (Routine => Check_Write_Offset'Access,
         Name    => "Check Blob_Write with offset");
      T.Add_Test_Routine
        (Routine => Check_Read'Access,
         Name    => "Check Blob_Read");
      T.Add_Test_Routine
        (Routine => Check_Read_Offset'Access,
         Name    => "Check Blob_Read with offset");
      T.Add_Test_Routine
        (Routine => Check_Reset'Access,
         Name    => "Check Blob_Reset");
   end Initialize;

end Server_Ike_Blob_Tests;
